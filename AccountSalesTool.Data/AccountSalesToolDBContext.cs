﻿using AccountSalesTool.Core.Models;
using AccountSalesTool.Data.Configurations;
using Microsoft.EntityFrameworkCore;

namespace AccountSalesTool.Data
{
    public class AccountSalesToolDBContext : DbContext
    {
        public DbSet<Office> Offices { get; set; }
        public DbSet<Sale> Sales { get; set; }
        public DbSet<Source> Sources { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<User> Users { get; set; }

        public AccountSalesToolDBContext(DbContextOptions<AccountSalesToolDBContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new OfficeConfiguration());
            builder.ApplyConfiguration(new OfficeSourceConfiguration());
            builder.ApplyConfiguration(new OfficeUserConfiguration());
            builder.ApplyConfiguration(new SaleConfiguration());
            builder.ApplyConfiguration(new SourceConfiguration());
            builder.ApplyConfiguration(new StateConfiguration());
            builder.ApplyConfiguration(new UserConfiguration());
        }
    }
}
