﻿using AccountSalesTool.Core;
using AccountSalesTool.Core.Repositories;
using AccountSalesTool.Data.Repositories;
using System.Threading.Tasks;

namespace AccountSalesTool.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AccountSalesToolDBContext _context;

        private IOfficeRepository _officeRepository;
        private IOfficeUserRepository _officeUserRepository;
        private ISaleRepository _saleRepository;
        private ISourceRepository _sourceRepository;
        private IStateRepository _stateRepository;
        private IUserRepository _userRepository;

        public UnitOfWork(AccountSalesToolDBContext context) => _context = context;

        public IOfficeRepository Offices => _officeRepository ??= new OfficeRepository(_context);
        public IOfficeUserRepository OfficeUserRelationships => _officeUserRepository ??= new OfficeUserRepository(_context);
        public ISaleRepository Sales => _saleRepository ??= new SaleRepository(_context);
        public ISourceRepository Sources => _sourceRepository ??= new SourceRepository(_context);
        public IStateRepository States => _stateRepository ??= new StateRepository(_context);
        public IUserRepository Users => _userRepository ??= new UserRepository(_context);

        public async Task<int> CommitAsync() => await _context.SaveChangesAsync();

        public void Dispose() => _context.Dispose();
    }
}
