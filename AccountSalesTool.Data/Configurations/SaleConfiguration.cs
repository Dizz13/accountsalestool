﻿using AccountSalesTool.Core.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AccountSalesTool.Data.Configurations
{
    public class SaleConfiguration : BaseConfiguration<Sale>
    {
        public override void Configure(EntityTypeBuilder<Sale> builder)
        {
            base.Configure(builder);

            builder
                .Property(s => s.Phone)
                .IsRequired()
                .HasMaxLength(128);

            builder
                .Property(s => s.Email)
                .IsRequired()
                .HasMaxLength(128);
        }
    }
}
