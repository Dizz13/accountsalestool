﻿using AccountSalesTool.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AccountSalesTool.Data.Configurations
{
    public class OfficeConfiguration : BaseConfiguration<Office>
    {
        public override void Configure(EntityTypeBuilder<Office> builder)
        {
            base.Configure(builder);

            builder
                .Property(o => o.Name)
                .IsRequired()
                .HasMaxLength(50);

            builder
                .HasMany(s => s.Sales)
                .WithOne(s => s.Office)
                .HasForeignKey(s => s.OfficeId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
