﻿using AccountSalesTool.Core.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AccountSalesTool.Data.Configurations
{
    public class UserConfiguration : BaseConfiguration<User>
    {
        public override void Configure(EntityTypeBuilder<User> builder)
        {
            base.Configure(builder);

            builder
                .Property(a => a.Email)
                .IsRequired()
                .HasMaxLength(128);

            builder
                .Property(a => a.Name)
                .IsRequired()
                .HasMaxLength(50);

            builder
                .Property(a => a.PasswordHash)
                .IsRequired();

            builder
                .Property(a => a.Type)
                .IsRequired();
        }
    }
}
