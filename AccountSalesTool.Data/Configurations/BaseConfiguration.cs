﻿using AccountSalesTool.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AccountSalesTool.Data.Configurations
{
    public abstract class BaseConfiguration<TEntity> : IEntityTypeConfiguration<TEntity> where TEntity : EntityBase, new()
    {
        public virtual void Configure(EntityTypeBuilder<TEntity> builder)
        {
            builder
                .HasKey(e => e.Id);

            builder
                .Property(e => e.Timestamp)
                .IsRowVersion();

            builder
                .Property(e => e.CreatedOn)
                .IsRequired();

            builder
                .Property(e => e.UpdatedOn)
                .IsRequired();
        }
    }
}
