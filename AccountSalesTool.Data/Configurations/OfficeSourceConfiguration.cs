﻿using AccountSalesTool.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AccountSalesTool.Data.Configurations
{
    public class OfficeSourceConfiguration : IEntityTypeConfiguration<OfficeSource>
    {
        public void Configure(EntityTypeBuilder<OfficeSource> builder)
        {
            builder
                .HasKey(os => new { os.OfficeId, os.SourceId });

            builder
                .HasOne(os => os.Office)
                .WithMany(o => o.Sources)
                .HasForeignKey(o => o.OfficeId)
                .OnDelete(DeleteBehavior.Cascade);

            builder
                .HasOne(os => os.Source)
                .WithMany(o => o.Offices)
                .HasForeignKey(o => o.SourceId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
