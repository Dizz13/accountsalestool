﻿using AccountSalesTool.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AccountSalesTool.Data.Configurations
{
    public class SourceConfiguration : BaseConfiguration<Source>
    {
        public override void Configure(EntityTypeBuilder<Source> builder)
        {
            base.Configure(builder);

            builder
                .Property(s => s.Name)
                .IsRequired()
                .HasMaxLength(50);

            builder
                .HasMany(s => s.Sales)
                .WithOne(s => s.Source)
                .HasForeignKey(s => s.SourceId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
