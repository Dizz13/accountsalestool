﻿using AccountSalesTool.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AccountSalesTool.Data.Configurations
{
    public class OfficeUserConfiguration : IEntityTypeConfiguration<OfficeUser>
    {
        public void Configure(EntityTypeBuilder<OfficeUser> builder)
        {
            builder
                .HasKey(os => new { os.OfficeId, os.UserId });

            builder
                .HasIndex(ou => ou.OfficeId)
                .IsUnique();

            builder
                .HasIndex(ou => ou.UserId)
                .IsUnique();
        }
    }
}
