﻿using AccountSalesTool.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AccountSalesTool.Data.Configurations
{
    public class StateConfiguration : BaseConfiguration<State>
    {
        public override void Configure(EntityTypeBuilder<State> builder)
        {
            base.Configure(builder);

            builder
                .Property(s => s.Name)
                .IsRequired()
                .HasMaxLength(20);

            builder
                .Property(s => s.Code)
                .IsRequired()
                .HasMaxLength(2);

            builder
                .HasIndex(s => s.Name)
                .IsUnique();

            builder
                .HasIndex(s => s.Code)
                .IsUnique();

            builder
                .HasMany(s => s.Sales)
                .WithOne(s => s.State)
                .HasForeignKey(s => s.StateId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
