﻿using AccountSalesTool.Core.Enums;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Security.Cryptography;
using System.Text;

namespace AccountSalesTool.Data.Migrations
{
    public partial class AddDefaultAdminUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var email = "admin@default";
            var name = "Default Admin User";
            var password = "admin";
            InsertAdminUser(migrationBuilder, name, email, password);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM [Users]", true);
        }

        private void InsertAdminUser(MigrationBuilder migrationBuilder, string name, string email, string password)
        {
            var id = Guid.NewGuid();
            var hash = GetPasswordHash(id, password);

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] {
                    "Id",
                    "CreatedOn",
                    "UpdatedOn",
                    "Email",
                    "Name",
                    "PasswordHash",
                    "Type"
                },
                values: new object[] {
                    id,
                    DateTime.UtcNow,
                    DateTime.UtcNow,
                    email,
                    name,
                    hash,
                    0
                });
        }

        private string GetPasswordHash(Guid id, string password)
        {
            var salt = Encoding.UTF8.GetBytes(id.ToString());
            var value = Encoding.UTF8.GetBytes(password);
            var hmac = new HMACMD5(salt);
            return Convert.ToBase64String(hmac.ComputeHash(value));
        }
    }
}
