﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Linq;

namespace AccountSalesTool.Data.Migrations
{
    public partial class PopulateStates : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            InsertUSStates(migrationBuilder);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM [States]", true);
        }

        private void InsertUSStates(MigrationBuilder migrationBuilder)
        {
            var states = new[]
            {
                new { Name = "Alabama", Code = "AL" },
                new { Name = "Alaska", Code = "AK" },
                new { Name = "Arizona", Code = "AZ" },
                new { Name = "Arkansas", Code = "AR" },
                new { Name = "California", Code = "CA" },
                new { Name = "Colorado", Code = "CO" },
                new { Name = "Connecticut", Code = "CT" },
                new { Name = "Delaware", Code = "DE" },
                new { Name = "Florida", Code = "FL" },
                new { Name = "Georgia", Code = "GA" },
                new { Name = "Hawaii", Code = "HI" },
                new { Name = "Idaho", Code = "ID" },
                new { Name = "Illinois", Code = "IL" },
                new { Name = "Indiana", Code = "IN" },
                new { Name = "Iowa", Code = "IA" },
                new { Name = "Kansas", Code = "KS" },
                new { Name = "Kentucky", Code = "KY" },
                new { Name = "Louisiana", Code = "LA" },
                new { Name = "Maine", Code = "ME" },
                new { Name = "Maryland", Code = "MD" },
                new { Name = "Massachusetts", Code = "MA" },
                new { Name = "Michigan", Code = "MI" },
                new { Name = "Minnesota", Code = "MN" },
                new { Name = "Mississippi", Code = "MS" },
                new { Name = "Missouri", Code = "MO" },
                new { Name = "Montana", Code = "MT" },
                new { Name = "Nebraska", Code = "NE" },
                new { Name = "Nevada", Code = "NV" },
                new { Name = "New Hampshire", Code = "NH" },
                new { Name = "New Jersey", Code = "NJ" },
                new { Name = "New Mexico", Code = "NM" },
                new { Name = "New York", Code = "NY" },
                new { Name = "North Carolina", Code = "NC" },
                new { Name = "North Dakota", Code = "ND" },
                new { Name = "Ohio", Code = "OH" },
                new { Name = "Oklahoma", Code = "OK" },
                new { Name = "Oregon", Code = "OR" },
                new { Name = "Pennsylvania", Code = "PA" },
                new { Name = "Rhode Island", Code = "RI" },
                new { Name = "South Carolina", Code = "SC" },
                new { Name = "South Dakota", Code = "SD" },
                new { Name = "Tennessee", Code = "TN" },
                new { Name = "Texas", Code = "TX" },
                new { Name = "Utah", Code = "UT" },
                new { Name = "Vermont", Code = "VT" },
                new { Name = "Virginia", Code = "VA" },
                new { Name = "Washington", Code = "WA" },
                new { Name = "West Virginia", Code = "WV" },
                new { Name = "Wisconsin", Code = "WI" },
                new { Name = "Wyoming", Code = "WY" }
            }.ToList();

            foreach (var state in states)
                InsertState(migrationBuilder, state.Name, state.Code);
        }

        private void InsertState(MigrationBuilder migrationBuilder, string name, string code)
        {
            migrationBuilder.InsertData(
                table: "States",
                columns: new[] {
                    "Id",
                    "CreatedOn",
                    "UpdatedOn",
                    "Name",
                    "Code"
                },
                values: new object[] {
                    Guid.NewGuid(),
                    DateTime.UtcNow,
                    DateTime.UtcNow,
                    name,
                    code
                });
        }
    }
}
