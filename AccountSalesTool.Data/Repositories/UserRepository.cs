﻿using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Repositories;

namespace AccountSalesTool.Data.Repositories
{
    public class UserRepository : Repository<AccountSalesToolDBContext, User>, IUserRepository
    {
        public UserRepository(AccountSalesToolDBContext context)
            : base(context)
        { }
    }
}
