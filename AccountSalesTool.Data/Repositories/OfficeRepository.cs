﻿using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountSalesTool.Data.Repositories
{
    public class OfficeRepository : Repository<AccountSalesToolDBContext, Office>, IOfficeRepository
    {
        public OfficeRepository(AccountSalesToolDBContext context)
            : base(context)
        { }

        public override async Task<IEnumerable<Office>> GetAllAsync() => await Context.Offices.OrderBy(s => s.Name).ToListAsync();
    }
}
