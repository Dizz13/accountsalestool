﻿using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountSalesTool.Data.Repositories
{
    public class StateRepository : Repository<AccountSalesToolDBContext, State>, IStateRepository
    {
        public StateRepository(AccountSalesToolDBContext context)
            : base(context)
        { }

        public override async Task<IEnumerable<State>> GetAllAsync() => await Context.States.OrderBy(s => s.Name).ToListAsync();
    }
}
