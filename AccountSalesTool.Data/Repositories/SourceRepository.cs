﻿using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountSalesTool.Data.Repositories
{
    public class SourceRepository : Repository<AccountSalesToolDBContext, Source>, ISourceRepository
    {
        public SourceRepository(AccountSalesToolDBContext context)
            : base(context)
        { }

        public override async Task<IEnumerable<Source>> GetAllAsync() => await Context.Sources.OrderBy(s => s.Name).ToListAsync();
    }
}
