﻿using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Repositories;

namespace AccountSalesTool.Data.Repositories
{
    public class SaleRepository : Repository<AccountSalesToolDBContext, Sale>, ISaleRepository
    {
        public SaleRepository(AccountSalesToolDBContext context)
            : base(context)
        { }
    }
}
