﻿using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace AccountSalesTool.Data.Repositories
{
    public abstract class Repository<TContext, TEntity> : IRepository<TEntity>
        where TContext : DbContext
        where TEntity : EntityBase, new()
    {
        protected readonly TContext Context;

        public Repository(TContext context) => Context = context;

        public async Task AddAsync(TEntity entity)
        {
            SetEntityId(entity);
            await Context.Set<TEntity>().AddAsync(entity);
        }

        public async Task AddRangeAsync(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
                SetEntityId(entity);
            await Context.Set<TEntity>().AddRangeAsync(entities);
        }

        public async Task<int> CountAllAsync() => await Context.Set<TEntity>().CountAsync();
        public async Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate) => await Context.Set<TEntity>().Where(predicate).CountAsync();
        public async Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate) => await Context.Set<TEntity>().Where(predicate).ToListAsync();
        public virtual async Task<IEnumerable<TEntity>> GetAllAsync() => await Context.Set<TEntity>().ToListAsync();
        public ValueTask<TEntity> GetByIdAsync(Guid id) => Context.Set<TEntity>().FindAsync(id);
        public void Remove(TEntity entity) => Context.Set<TEntity>().Remove(entity);
        public void RemoveRange(IEnumerable<TEntity> entities) => Context.Set<TEntity>().RemoveRange(entities);
        public Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate) => Context.Set<TEntity>().SingleOrDefaultAsync(predicate);
        public void Update(TEntity entity) => Context.Set<TEntity>().Update(entity);
        public void UpdateRange(IEnumerable<TEntity> entities) => Context.Set<TEntity>().UpdateRange(entities);

        public async Task<IEnumerable<TEntity>> FindAllAsync(Expression<Func<TEntity, bool>> match) => await Context.Set<TEntity>().Where(match).ToListAsync();
        public async Task<IEnumerable<TEntity>> FindAllAsync(Expression<Func<TEntity, bool>> match, int take) => await Context.Set<TEntity>().Where(match).Take(take).ToListAsync();
        public async Task<IEnumerable<TEntity>> FindAllAsync(Expression<Func<TEntity, bool>> match, int take, int skip) => await Context.Set<TEntity>().Where(match).Skip(skip).Take(take).ToListAsync();

        public async Task<IEnumerable<TEntity>> FindAllOrderedAsync(Expression<Func<TEntity, bool>> match, Expression<Func<TEntity, object>> orderBy, bool isAscending)
        {
            IOrderedQueryable<TEntity> orderedQuery;
            var query = Context.Set<TEntity>().Where(match);
            orderedQuery = isAscending ? query.OrderBy(orderBy) : query.OrderByDescending(orderBy);
            return await orderedQuery.ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> FindAllOrderedAsync(Expression<Func<TEntity, bool>> match, Expression<Func<TEntity, object>> orderBy, bool isAscending, int take)
        {
            IOrderedQueryable<TEntity> orderedQuery;
            var query = Context.Set<TEntity>().Where(match);
            orderedQuery = isAscending ? query.OrderBy(orderBy) : query.OrderByDescending(orderBy);
            return await orderedQuery.Take(take).ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> FindAllOrderedAsync(Expression<Func<TEntity, bool>> match, Expression<Func<TEntity, object>> orderBy, bool isAscending, int take, int skip)
        {
            IOrderedQueryable<TEntity> orderedQuery;
            var query = Context.Set<TEntity>().Where(match);
            orderedQuery = isAscending ? query.OrderBy(orderBy) : query.OrderByDescending(orderBy);
            return await orderedQuery.Skip(skip).Take(take).ToListAsync();
        }

        private void SetEntityId(TEntity entity)
        {
            if (entity.Id == Guid.Empty)
                entity.Id = Guid.NewGuid();
        }
    }
}
