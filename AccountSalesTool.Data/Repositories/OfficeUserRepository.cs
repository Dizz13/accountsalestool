﻿using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AccountSalesTool.Data.Repositories
{
    public class OfficeUserRepository : IOfficeUserRepository
    {
        private readonly AccountSalesToolDBContext _context;

        public OfficeUserRepository(AccountSalesToolDBContext context) => _context = context;

        public async Task AddAsync(OfficeUser relationship) => await _context.Set<OfficeUser>().AddAsync(relationship);
        public void Remove(OfficeUser relationship) => _context.Set<OfficeUser>().Remove(relationship);
        public Task<OfficeUser> SingleOrDefaultAsync(Expression<Func<OfficeUser, bool>> predicate) => _context.Set<OfficeUser>().SingleOrDefaultAsync(predicate);
    }
}
