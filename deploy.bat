@echo off

echo ################
echo ### Stop IIS ###
echo ################
iisreset /stop
echo ####################
echo ### IIS Stopped ####
echo ####################
pause

echo ####################
echo ### Remove files ###
echo ####################
pushd AccountSalesTool.Client
rmdir /S /Q dist
popd

pushd AccountSalesTool.Client 
rmdir /S /Q node_modules 
popd

pushd AccountSalesTool.Initializer 
rmdir /S /Q bin 
popd

pushd AccountSalesTool.Initializer 
rmdir /S /Q obj 
popd

pushd AccountSalesTool.Web 
rmdir /S /Q bin 
popd

pushd AccountSalesTool.Web 
rmdir /S /Q obj 
popd

pushd c:\inetpub\wwwroot
rmdir /S /Q accountsalestool
popd
echo ######################
echo ### Files removed ####
echo ######################

pause

echo ######################
echo ### Dotnet restore ###
echo ######################
dotnet restore
echo ##########################
echo ### Dotnet restore end ###
echo ##########################
pause

echo ##########################
echo ### Dotnet publish Web ###
echo ##########################
dotnet publish --output c:\inetpub\wwwroot\accountsalestool\
echo ##############################
echo ### Dotnet publish Web End ###
echo ##############################
pause

echo ###################
echo ### npm install ###
echo ###################
cd AccountSalesTool.Client 
call npm install 
cd ..
echo #######################
echo ### npm install end ###
echo #######################
pause

echo ####################
echo ### client build ###
echo ####################
pushd AccountSalesTool.Client 
call npm run buildqa 
popd
echo ########################
echo ### client build end ###
echo ########################
pause

echo #########################
echo ### Initializer build ###
echo #########################
dotnet build AccountSalesTool.Initializer\AccountSalesTool.Initializer.csproj
echo #############################
echo ### Initializer build end ###
echo #############################
pause

echo ###################
echo ### Initializer ###
echo ###################
pushd AccountSalesTool.Initializer\bin\Debug\netcoreapp3.1
call AccountSalesTool.Initializer.exe 1
popd
echo #######################
echo ### Initializer end ###
echo #######################
pause

echo #################
echo ### iis start ###
echo #################
iisreset /start
echo ###################
echo ### iis started ###
echo ###################
pause