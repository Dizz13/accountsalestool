﻿namespace AccountSalesTool.Web.Api
{
    public class AppSettings
    {
        public string JWTSecret { get; set; }
        public string JWTIssuer { get; set; }
        public string JWTAudience { get; set; }
        public string ClientURL { get; set; }
    }
}
