using AccountSalesTool.Core;
using AccountSalesTool.Core.Services;
using AccountSalesTool.Data;
using AccountSalesTool.Services;
using AccountSalesTool.Web.Api;
using AccountSalesTool.Web.Api.Auth;
using AccountSalesTool.Web.Api.Exceptions;
using AccountSalesTool.Web.Api.Hubs;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using NLog;
using System;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace AccountSalesTool.Web
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration) => Configuration = configuration;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IOfficeService, OfficeService>();
            services.AddTransient<IOfficeUserService, OfficeUserService>();
            services.AddTransient<ISaleService, SaleService>();
            services.AddTransient<ISourceService, SourceService>();
            services.AddTransient<IStateService, StateService>();
            services.AddTransient<IUserService, UserService>();

            services.AddAutoMapper(typeof(Startup));

            services.AddDbContext<AccountSalesToolDBContext>(options => options.UseLazyLoadingProxies().UseSqlServer(Configuration.GetConnectionString("Default")));

            services.AddControllers();

            services.AddCors();

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false; // enable for https
                options.SaveToken = false;

                var key = Encoding.UTF8.GetBytes(Configuration["AppSettings:JWTSecret"].ToString());

                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = true,
                    ValidIssuer = Configuration["AppSettings:JWTIssuer"].ToString(),
                    ValidateAudience = true,
                    ValidAudience = Configuration["AppSettings:JWTAudience"].ToString(), // will be expanded for ticker support
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero
                };

                options.Events = new JwtBearerEvents
                {
                    OnMessageReceived = context =>
                    {
                        var accessToken = context.Request.Query["access_token"];

                        var path = context.HttpContext.Request.Path;
                        if (!string.IsNullOrEmpty(accessToken) && path.StartsWithSegments("/ticker"))
                            context.Token = accessToken;
                        return Task.CompletedTask;
                    }
                };
            });

            services.AddAuthorization(config =>
            {
                config.AddPolicy(Policies.Admin, Policies.AdminPolicy());
                config.AddPolicy(Policies.Office, Policies.OfficePolicy());
                config.AddPolicy(Policies.Report, Policies.ReportPolicy());
            });

            services.AddControllers().AddJsonOptions(
                options => { 
                    options.JsonSerializerOptions.Converters.Add(new DateTimeConverter());
                });

            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            var logger = LogManager.GetCurrentClassLogger();
            logger.Info($"Configuration: {env.EnvironmentName}");

            string logDirectory = Configuration.GetSection("LogsPath").Value;
            GlobalDiagnosticsContext.Set("configDir", logDirectory);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.ConfigureExceptionHandler();

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseCors(builder =>
            {
                //builder.AllowAnyOrigin();
                builder.WithOrigins(Configuration["AppSettings:ClientURL"].ToString())
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials();
            });

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<TickerHub>("/ticker");
            });

            app.Run(async (context) =>
            {
                context.Response.ContentType = "text/html";
                await context.Response.SendFileAsync(Path.Combine(env.WebRootPath, "index.html"));
            });
        }

        public class DateTimeConverter : JsonConverter<DateTime>
        {
            public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
            {
                return DateTime.Parse(reader.GetString());
            }

            public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
            {
                writer.WriteStringValue(value.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ssZ"));
            }
        }

    }
}
