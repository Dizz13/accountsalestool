﻿using AccountSalesTool.Core.Services;
using AccountSalesTool.Web.Api.Auth;
using AccountSalesTool.Web.Api.DTO.Offices;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace AccountSalesTool.Web.Api.Hubs
{
    [Authorize(Policy = Policies.Report)]
    public class TickerHub : Hub
    {
        private readonly IOfficeService _officeService;
        private readonly IMapper _mapper;

        public TickerHub(IOfficeService officeService, IMapper mapper)
        {
            _officeService = officeService;
            _mapper = mapper;
        }

        public override async Task OnConnectedAsync()
        {
            var offices = await _officeService.GetAllAsync();
            var result = offices
                .Select(c => _mapper.Map<OfficeTickerDTO>(c))
                .ToList();
            await Clients.Caller.SendAsync("data", result);
            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            await Clients.All.SendAsync("Notify", $"{Context.ConnectionId} покинул в чат");
            await base.OnDisconnectedAsync(exception);
        }
    }
}
