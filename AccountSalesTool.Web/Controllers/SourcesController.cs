﻿using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Services;
using AccountSalesTool.Web.Api.Auth;
using AccountSalesTool.Web.Api.DTO;
using AccountSalesTool.Web.Api.DTO.Sales;
using AccountSalesTool.Web.Api.DTO.Sources;
using AccountSalesTool.Web.Api.Models.Sources;
using AccountSalesTool.Web.Api.Validators.Sources;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using AccountSalesTool.Web.Api.Models;
using AccountSalesTool.Core.Pagination;
using AccountSalesTool.Services;

namespace AccountSalesTool.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SourcesController : ControllerBase
    {
        private readonly ISourceService _sourceService;
        private readonly IMapper _mapper;

        public SourcesController(ISourceService sourceService, IMapper mapper)
        {
            _sourceService = sourceService;
            _mapper = mapper;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAll()
        {
            var sources = await _sourceService.GetAllAsync();
            var result = sources
                .Select(c => _mapper.Map<SourceListDTO>(c))
                .ToList();
            return Ok(result);
        }

        [HttpGet("count")]
        public async Task<IActionResult> GetCount()
        {
            var result = await _sourceService.CountAsync();
            return Ok(result);
        }

        [HttpGet("by-office/{id:guid}")]
        [Authorize(Policy = Policies.Office)]
        public async Task<IActionResult> GetSourcesByOffice(Guid id)
        {
            var sources = await _sourceService.GetByOffice(id);
            var result = sources
                .Select(c => _mapper.Map<SelectResultDTO>(c))
                .OrderBy(c => c.Name)
                .ToList();
            return Ok(result);
        }

        [HttpPost]
        [Route("")]
        [Authorize(Policy = Policies.Admin)]
        public async Task<IActionResult> CreateSource(AddSourceModel model)
        {
            var validator = new AddSourceModelValidator(_sourceService);
            var validationResult = await validator.ValidateAsync(model);

            if (!validationResult.IsValid)
            {
                var badResult = _mapper.Map(validationResult.Errors, new BadRequestDTO());
                return BadRequest(badResult);
            }

            var source = _mapper.Map<Source>(model);
            var id = await _sourceService.CreateAsync(source);

            return Ok(new IdResultDTO(id));
        }

        [HttpGet("{id:guid}")]
        [Authorize(Policy = Policies.Admin)]
        public async Task<IActionResult> Get(Guid id)
        {
            var source = await _sourceService.GetByIdAsync(id);
            var result = _mapper.Map(source, new SourceEditDTO());
            return Ok(result);
        }

        [HttpPut("{id:guid}")]
        [Authorize(Policy = Policies.Admin)]
        public async Task<IActionResult> Update(Guid id, EditSourceModel model)
        {
            var source = await _sourceService.GetByIdAsync(id);
            if (source == null)
                return BadRequest(new BadRequestDTO("Can not find Source with that Id"));

            var validator = new EditSourceModelValidator(_sourceService);
            var validationResult = await validator.ValidateAsync(model);

            if (!validationResult.IsValid)
            {
                var badResult = _mapper.Map(validationResult.Errors, new BadRequestDTO());
                return BadRequest(badResult);
            }

            _mapper.Map(model, source);
            id = await _sourceService.UpdateAsync(source);
            return Ok(new IdResultDTO(id));
        }

        [HttpGet("for-selection")]
        public async Task<IActionResult> GetForSelection()
        {
            var sources = await _sourceService.GetAllAsync();
            var result = sources
                .Select(c => _mapper.Map<SelectResultDTO>(c))
                .ToList();
            return Ok(result);
        }

        [HttpDelete("{id:guid}")]
        [Authorize(Policy = Policies.Admin)]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await _sourceService.DeleteAsync(id);
            if (!result)
                return BadRequest(new BadRequestDTO("Can not find source with that Id"));

            return Ok();
        }

        [HttpPost("page")]
        [Authorize(Policy = Policies.Admin)]
        public async Task<IActionResult> GetPage(QueryPageModel model)
        {
            var query = _mapper.Map<IQueryPage<Source>>(model);
            var page = await _sourceService.GetPageAsync(query);
            var data = page.Data.Select(u => _mapper.Map<SourceListDTO>(u));
            var result = new Page<SourceListDTO>(data, page.Number, page.Size, page.Total);
            return Ok(result);
        }

    }
}