﻿using AccountSalesTool.Core.Services;
using AccountSalesTool.Web.Api.DTO;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace AccountSalesTool.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class StatesController : ControllerBase
    {
        private readonly IStateService _stateService;
        private readonly IMapper _mapper;

        public StatesController(IStateService stateService, IMapper mapper)
        {
            _stateService = stateService;
            _mapper = mapper;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAll()
        {
            var states = await _stateService.GetAllAsync();
            var result = states
                .Select(c => _mapper.Map<SelectResultDTO>(c))
                .ToList();
            return Ok(result);
        }
    }
}
