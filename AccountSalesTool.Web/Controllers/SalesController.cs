﻿using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Pagination;
using AccountSalesTool.Core.Services;
using AccountSalesTool.Services;
using AccountSalesTool.Web.Api.Auth;
using AccountSalesTool.Web.Api.DTO;
using AccountSalesTool.Web.Api.DTO.Sales;
using AccountSalesTool.Web.Api.Hubs;
using AccountSalesTool.Web.Api.Models.Sales;
using AccountSalesTool.Web.Api.Validators.Sales;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AccountSalesTool.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SalesController : ControllerBase
    {
        private readonly ISaleService _saleService;
        private readonly IOfficeService _officeService;
        private readonly ISourceService _sourceService;
        private readonly IStateService _stateService;
            
        private readonly IMapper _mapper; 
        private readonly IHubContext<TickerHub> _hub;

        public SalesController(ISaleService saleService, IOfficeService officeService, ISourceService sourceService, IStateService stateService,
            IMapper mapper, IHubContext<TickerHub> hub)
        {
            _saleService = saleService;
            _officeService = officeService;
            _sourceService = sourceService;
            _stateService = stateService;

            _mapper = mapper;
            _hub = hub;
        }

        [HttpGet("count")]
        public async Task<IActionResult> GetCount()
        {
            var result = await _saleService.CountAsync();
            return Ok(result);
        }

        [HttpGet("count-today/{officeId}")]
        [Authorize(Policy = Policies.Office)]
        public async Task<IActionResult> GetCountByOffice(Guid officeId)
        {
            var from = DateTime.Now.Date.ToUniversalTime();
            var to = DateTime.Now.Date.AddHours(23).AddMinutes(59).AddSeconds(59).ToUniversalTime();
            var result = await _saleService.GetSalesCountByOfficeAsync(officeId, from, to);
            return Ok(result);
        }


        [HttpPost]
        [Route("")]
        [Authorize(Policy = Policies.Office)]
        public async Task<IActionResult> CreateSale(AddSaleModel model)
        {
            var validator = new AddSaleModelValidator(_saleService);
            var validationResult = await validator.ValidateAsync(model);
            if (!validationResult.IsValid)
            {
                var badResult = _mapper.Map(validationResult.Errors, new BadRequestDTO());
                return BadRequest(badResult);
            }

            var sale = _mapper.Map<Sale>(model);
            var id = await _saleService.CreateAsync(sale);
            _hub.Clients.All.SendAsync("update", new IdResultDTO(sale.OfficeId));

            return Ok(new IdResultDTO(id));
        }

        [HttpPost("export-file")]
        [Authorize(Policy = Policies.Admin)]
        public async Task<FileResult> ExportFile(FilterSaleModel model) {

            var startDate = model.StartDate.ToUniversalTime().AddMinutes(-model.TimeZoneOffset).ToString("MM/dd/yyyy");
            var endDate = model.EndDate.ToUniversalTime().AddMinutes(-model.TimeZoneOffset).ToString("MM/dd/yyyy");

            model.StartDate = model.StartDate.ToUniversalTime();
            model.EndDate = model.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59).ToUniversalTime();
            model.PageNumber = 1;
            model.PageSize = int.MaxValue;

            var query = _mapper.Map<IQueryPage<Sale>>(model);
            var page = await _saleService.GetPageAsync(query);

            using var package = new ExcelPackage();
            var worksheet = package.Workbook.Worksheets.Add("List 1");

            worksheet.Cells[1, 1].Value = "Start Date";
            worksheet.Cells[2, 1].Value = "End Date";
            worksheet.Cells[3, 1].Value = "Office";
            worksheet.Cells[4, 1].Value = "Source";
            worksheet.Cells[5, 1].Value = "State";
            worksheet.Cells["A1:A5"].Style.Font.Bold = true;

            worksheet.Cells[1, 2].Value = startDate;
            worksheet.Cells[2, 2].Value = endDate;
            worksheet.Cells[3, 2].Value = model.OfficeId == null ? "All" : (await _officeService.GetByIdAsync(model.OfficeId.Value))?.Name;
            worksheet.Cells[4, 2].Value = model.SourceId == null ? "All" : (await _sourceService.GetByIdAsync(model.SourceId.Value))?.Name;
            worksheet.Cells[5, 2].Value = model.StateId == null ? "All" : (await _stateService.GetByIdAsync(model.StateId.Value))?.Name;


            worksheet.Cells[7, 1].Value = "Phone";
            worksheet.Cells[7, 2].Value = "Office";
            worksheet.Cells[7, 3].Value = "Source";
            worksheet.Cells[7, 4].Value = "Email";
            worksheet.Cells[7, 5].Value = "State";
            worksheet.Cells[7, 6].Value = "Date";
            worksheet.Cells[7, 7].Value = "Type";

            int index = 8;
            var sales = (await _saleService.GetAllAsync()).ToList();
            foreach (var sale in page.Data)
            {
                worksheet.Cells[index, 1].Value = sale.Phone;
                worksheet.Cells[index, 2].Value = sale.Office?.Name;
                worksheet.Cells[index, 3].Value = sale.Source?.Name;
                worksheet.Cells[index, 4].Value = sale.Email;
                worksheet.Cells[index, 5].Value = sale.State?.Name;
                worksheet.Cells[index, 6].Value = sale.CreatedOn.AddMinutes(-model.TimeZoneOffset).ToString("MM/dd/yyyy h:mm:ss tt");
                worksheet.Cells[index, 7].Value = "sale";

                index++;
            }

            worksheet.Cells["A7:G7"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            worksheet.Cells["A7:G7"].Style.Font.Bold = true;

            worksheet.Cells.AutoFitColumns();

            var stream = new MemoryStream();
            package.SaveAs(stream);

            stream.Seek(0, SeekOrigin.Begin);

            var contentType = "APPLICATION/octet-stream";
            return File(stream, contentType, "");
        }

        [HttpGet("")]
        [Authorize(Policy = Policies.Admin)]
        public async Task<IActionResult> GetAll()
        {
            var sales = await _saleService.GetAllAsync();
            var result = sales
                .Select(c => _mapper.Map<SaleListDTO>(c))
                .ToList();
            return Ok(result);
        }

        [HttpPost("page")]
        [Authorize(Policy = Policies.Admin)]
        public async Task<IActionResult> GetPage(FilterSaleModel model)
        {
            model.StartDate = model.StartDate.ToUniversalTime();
            model.EndDate = model.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59).ToUniversalTime();

            var query = _mapper.Map<IQueryPage<Sale>>(model);

            var page = await _saleService.GetPageAsync(query);
            var data = page.Data.Select(u => _mapper.Map<SaleListDTO>(u));
            var result = new Page<SaleListDTO>(data, page.Number, page.Size, page.Total);

            return Ok(result);
        }
    }
}
