﻿using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Services;
using AccountSalesTool.Web.Api.Auth;
using AccountSalesTool.Web.Api.DTO;
using AccountSalesTool.Web.Api.DTO.Offices;
using AccountSalesTool.Web.Api.Models.Offices;
using AccountSalesTool.Web.Api.Validators.Offices;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using AccountSalesTool.Web.Api.Models;
using AccountSalesTool.Core.Pagination;
using AccountSalesTool.Services;
using Microsoft.AspNetCore.SignalR;
using AccountSalesTool.Web.Api.Hubs;

namespace AccountSalesTool.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Policy = Policies.Admin)]
    public class OfficesController : ControllerBase
    {
        private readonly IOfficeService _officeService;
        private readonly IUserService _userService;
        private readonly IOfficeUserService _officeUserService;
        private readonly IMapper _mapper;
        private readonly IHubContext<TickerHub> _hub;

        public OfficesController(IOfficeService officeService, IUserService userService, IOfficeUserService officeUserService, IMapper mapper, IHubContext<TickerHub> hub)
        {
            _officeService = officeService;
            _userService = userService;
            _officeUserService = officeUserService;
            _mapper = mapper;
            _hub = hub;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAll()
        {
            var offices = await _officeService.GetAllAsync();
            var result = offices
                .Select(c => _mapper.Map<OfficeListDTO>(c))
                .OrderBy(c => c.Name)
                .ToList();
            return Ok(result);
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var office = await _officeService.GetByIdAsync(id);
            var result = _mapper.Map(office, new OfficeDetailsDTO());
            return Ok(result);
        }

        [HttpGet("{id:guid}/edit")]
        public async Task<IActionResult> GetEdit(Guid id)
        {
            var office = await _officeService.GetByIdAsync(id);
            var result = _mapper.Map(office, new OfficeEditDTO());
            return Ok(result);
        }

        [HttpGet("{id:guid}/sources")]
        public async Task<IActionResult> GetSources(Guid id)
        {
            var office = await _officeService.GetByIdAsync(id);
            var result = _mapper.Map(office, new OfficeSourcesDTO());
            return Ok(result);
        }

        [HttpPut("{id:guid}/sources")]
        public async Task<IActionResult> UpdateSources(Guid id, OfficeSourcesModel model)
        {
            var office = await _officeService.GetByIdAsync(id);
            if (office == null)
                return BadRequest(new BadRequestDTO("Can not find office with that Id"));

            _mapper.Map(model, office);
            id = await _officeService.UpdateAsync(office);

            return Ok(new IdResultDTO(id));
        }

        [HttpGet("count")]
        public async Task<IActionResult> GetCount()
        {
            var result = await _officeService.CountAsync();
            return Ok(result);
        }

        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreateOffice(AddOfficeModel model)
        {
            var validator = new AddOfficeModelValidator(_userService, _officeService);
            var validationResult = await validator.ValidateAsync(model);

            if (!validationResult.IsValid)
            {
                var badResult = _mapper.Map(validationResult.Errors, new BadRequestDTO());
                return BadRequest(badResult);
            }

            var office = _mapper.Map<Office>(model);
            var id = await _officeService.CreateAsync(office);

            await _officeUserService.DeleteRelationshipByOfficeAsync(id);
            if (model.UserId.HasValue)
                await _officeUserService.CreateRelationshipAsync(id, model.UserId.Value);

            await UpdateTickerOffices();

            return Ok(new IdResultDTO(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> Update(Guid id, EditOfficeModel model)
        {
            var office = await _officeService.GetByIdAsync(id);
            if (office == null)
                return BadRequest(new BadRequestDTO("Can not find office with that Id"));

            var validator = new EditOfficeModelValidator(_officeService);
            var validationResult = await validator.ValidateAsync(model);

            if (!validationResult.IsValid)
            {
                var badResult = _mapper.Map(validationResult.Errors, new BadRequestDTO());
                return BadRequest(badResult);
            }

            _mapper.Map(model, office);
            id = await _officeService.UpdateAsync(office);

            if (!model.UserId.HasValue)
                await _officeUserService.DeleteRelationshipByOfficeAsync(id);
            else if (model.UserId.Value != office.User?.UserId)
                await _officeUserService.CreateRelationshipAsync(id, model.UserId.Value);

            await UpdateTickerOffices();

            return Ok(new IdResultDTO(id));
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var result = await _officeService.DeleteAsync(id);

            await UpdateTickerOffices();

            if (!result)
                return BadRequest(new BadRequestDTO("Can not find office with that Id"));

            return Ok();
        }

        [HttpPost("page")]
        public async Task<IActionResult> GetPage(QueryPageModel model)
        {
            var query = _mapper.Map<IQueryPage<Office>>(model);
            var page = await _officeService.GetPageAsync(query);
            var data = page.Data.Select(u => _mapper.Map<OfficeListDTO>(u));
            var result = new Page<OfficeListDTO>(data, page.Number, page.Size, page.Total);
            return Ok(result);
        }

        [HttpGet("for-user")]
        public async Task<IActionResult> GetOfficesForUserWithoutCurrentAssignee() => await GetOfficesForUser();

        [HttpGet("for-user/{id:guid}")]
        public async Task<IActionResult> GetOfficesForUserWithCurrentAssignee(Guid id) => await GetOfficesForUser(id);

        private async Task<IActionResult> GetOfficesForUser(Guid? id = null)
        {
            var offices = await _officeService.GetNotAssignedUserOffices(id);
            var result = offices
                .Select(c => _mapper.Map<SelectResultDTO>(c))
                .OrderBy(c => c.Name)
                .ToList();
            return Ok(result);
        }

        private async Task UpdateTickerOffices()
        {
            var offices = await _officeService.GetAllAsync();
            var result = offices
                .Select(c => _mapper.Map<OfficeTickerDTO>(c))
                .ToList();
            await _hub.Clients.All.SendAsync("data", result);
        }
    }
}
