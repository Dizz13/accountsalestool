﻿using AccountSalesTool.Core.Enums;
using AccountSalesTool.Core.Services;
using AccountSalesTool.Web.Api.DTO;
using AccountSalesTool.Web.Api.DTO.Auth;
using AccountSalesTool.Web.Api.Hubs;
using AccountSalesTool.Web.Api.Models.Auth;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace AccountSalesTool.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly AppSettings _appSettings;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        private IHubContext<TickerHub> _hub;

        public AuthController(
            IOptions<AppSettings> appSettings,
            IUserService userService,
            IMapper mapper,
            IHubContext<TickerHub> hub
            )
        {
            _appSettings = appSettings.Value;
            _userService = userService;
            _mapper = mapper;
            _hub = hub;
        }

        [HttpPost]
        [Route("signin")]
        public async Task<IActionResult> SignInAsync([FromBody] SignInModel model)
        {
            var identity = await GetIdentityAsync(model.Email, model.Password);
            if (identity == null)
            {
                _hub.Clients.All.SendAsync("update", $"someone failed to login with {model.Email}");
                return BadRequest(new BadRequestDTO("Invalid email or password"));
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = identity,
                Issuer = _appSettings.JWTIssuer,
                Audience = _appSettings.JWTAudience, // fix audience
                Expires = DateTime.UtcNow.AddMonths(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.JWTSecret)), SecurityAlgorithms.HmacSha256Signature)
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var securityToken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(securityToken);

            var account = new AccountDTO()
            {
                Name = identity.Claims.First(c => c.Type == "Name").Value,
                Email = identity.Claims.First(c => c.Type == "Email").Value,
                Role = identity.Claims.First(c => c.Type == "role").Value,
                OfficeId = identity.Claims.FirstOrDefault(c => c.Type == "OfficeId")?.Value,
                Office = identity.Claims.FirstOrDefault(c => c.Type == "Office")?.Value,
                Token = token
            };

            return Ok(account);
        }

        private async Task<ClaimsIdentity> GetIdentityAsync(string email, string password)
        {
            var user = await _userService.GetByEmailAndPasswordAsync(email, password);
            if (user == null)
                return null;
            var claims = new List<Claim>();
            claims.Add(new Claim("UserId", user.Id.ToString()));
            claims.Add(new Claim("Name", user.Name));
            claims.Add(new Claim("Email", user.Email));
            claims.Add(new Claim("role", user.Type.ToString()));
            if (user.Type == UserTypeEnum.Office && user.Office != null)
            {
                claims.Add(new Claim("OfficeId", user.Office.OfficeId.ToString()));
                claims.Add(new Claim("Office", user.Office.Office.Name));
            }
            var identity = new ClaimsIdentity(claims);
            return identity;
        }
    }
}
