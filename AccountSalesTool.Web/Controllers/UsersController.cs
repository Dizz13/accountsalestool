﻿using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Pagination;
using AccountSalesTool.Core.Services;
using AccountSalesTool.Services;
using AccountSalesTool.Web.Api.Auth;
using AccountSalesTool.Web.Api.DTO;
using AccountSalesTool.Web.Api.DTO.Users;
using AccountSalesTool.Web.Api.Models;
using AccountSalesTool.Web.Api.Models.Users;
using AccountSalesTool.Web.Api.Validators.Users;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace AccountSalesTool.Web.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Policy = Policies.Admin)]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IOfficeUserService _officeUserService;
        private readonly IMapper _mapper;

        public UsersController(IUserService userService, IOfficeUserService officeUserService, IMapper mapper)
        {
            _userService = userService;
            _officeUserService = officeUserService;
            _mapper = mapper;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAll()
        {
            var users = await _userService.GetAllAsync();
            var result = users
                .Select(c => _mapper.Map<UserListDTO>(c))
                .ToList();
            return Ok(result);
        }

        [HttpPost("page")]
        public async Task<IActionResult> GetPage(QueryPageModel model)
        {
            var query = _mapper.Map<IQueryPage<User>>(model);
            var page = await _userService.GetPageAsync(query);
            var data = page.Data.Select(u => _mapper.Map<UserListDTO>(u));
            var result = new Page<UserListDTO>(data, page.Number, page.Size, page.Total);
            return Ok(result);
        }

        [HttpGet("count")]
        public async Task<IActionResult> GetCount()
        {
            var result = await _userService.CountAsync();
            return Ok(result);
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var user = await _userService.GetByIdAsync(id);
            var result = _mapper.Map(user, new UserEditDTO());
            return Ok(result);
        }

        [HttpPost]
        [Route("")]
        public async Task<IActionResult> CreateUser(AddUserModel model)
        {
            var validator = new AddUserModelValidator(_userService);
            var validationResult = await validator.ValidateAsync(model);

            if (!validationResult.IsValid)
            {
                var badResult = _mapper.Map(validationResult.Errors, new BadRequestDTO());
                return BadRequest(badResult);
            }

            var user = _mapper.Map<User>(model);
            var id = await _userService.CreateAsync(user);

            await _officeUserService.DeleteRelationshipByUserAsync(id);
            if (model.OfficeId.HasValue)
                await _officeUserService.CreateRelationshipAsync(model.OfficeId.Value, id);

            return Ok(new IdResultDTO(id));
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> Update(Guid id, EditUserModel model)
        {
            var currentUserId = new Guid(User.Claims.First(c => c.Type == "UserId").Value);

            var user = await _userService.GetByIdAsync(id);
            if (user == null)
                return BadRequest(new BadRequestDTO("Can not find user with that Id"));

            var validator = new EditUserModelValidator(_userService, user, currentUserId);
            var validationResult = await validator.ValidateAsync(model);

            if (!validationResult.IsValid)
            {
                var badResult = _mapper.Map(validationResult.Errors, new BadRequestDTO());
                return BadRequest(badResult);
            }

            _mapper.Map(model, user);
            id = await _userService.UpdateAsync(user, false);

            if (!model.OfficeId.HasValue)
                await _officeUserService.DeleteRelationshipByUserAsync(id);
            else if(model.OfficeId.Value != user.Office?.OfficeId)
                await _officeUserService.CreateRelationshipAsync(model.OfficeId.Value, id);

            return Ok(new IdResultDTO(id));
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var currentUserId = new Guid(User.Claims.First(c => c.Type == "UserId").Value);

            var validator = new DeleteUserValidator(currentUserId);
            var validationResult = await validator.ValidateAsync(id);

            if (!validationResult.IsValid)
            {
                var badResult = _mapper.Map(validationResult.Errors, new BadRequestDTO());
                return BadRequest(badResult);
            }

            var result = await _userService.DeleteAsync(id);
            if (!result)
                return BadRequest(new BadRequestDTO("Can not find user with that Id"));

            return Ok();
        }

        [HttpGet("{id:guid}/change-password")]
        public async Task<IActionResult> ChangePassword(Guid id)
        {
            var user = await _userService.GetByIdAsync(id);
            var result = _mapper.Map(user, new ChangePasswordDTO());
            return Ok(result);
        }

        [HttpPut("{id:guid}/change-password")]
        public async Task<IActionResult> ChangePassword(Guid id, ChangePasswordModel model)
        {
            var user = await _userService.GetByIdAsync(id);
            if (user == null)
                return BadRequest(new BadRequestDTO("Can not find user with that Id"));

            var validator = new ChangePasswordModelValidator();
            var validationResult = await validator.ValidateAsync(model);

            if (!validationResult.IsValid)
            {
                var badResult = _mapper.Map(validationResult.Errors, new BadRequestDTO());
                return BadRequest(badResult);
            }

            _mapper.Map(model, user);
            id = await _userService.UpdateAsync(user, true);

            return Ok(new IdResultDTO(id));
        }

        [HttpGet("for-office")]
        public async Task<IActionResult> GetUsersForOfficeWithoutCurrentAssignee() => await GetUsersForOffice();

        [HttpGet("for-office/{id:guid}")]
        public async Task<IActionResult> GetUsersForOfficeWithCurrentAssignee(Guid id) => await GetUsersForOffice(id);

        private async Task<IActionResult> GetUsersForOffice(Guid? id = null)
        {
            var users = await _userService.GetNotAssignedOfficeUsers(id);
            var result = users
                .Select(c => _mapper.Map<SelectResultDTO>(c))
                .ToList();
            return Ok(result);
        }
    }
}
