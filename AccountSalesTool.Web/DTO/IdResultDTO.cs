﻿using System;

namespace AccountSalesTool.Web.Api.DTO
{
    public class IdResultDTO
    {
        public Guid Id { get; set; }

        public IdResultDTO(Guid id) => Id = id;
    }
}
