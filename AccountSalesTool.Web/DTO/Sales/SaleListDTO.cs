﻿using System;

namespace AccountSalesTool.Web.Api.DTO.Sales
{
    public class SaleListDTO
    {
        public Guid Id { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string State { get; set; }
        public string Source { get; set; }
        public string Office { get; set; }
        public string Type { get { return "sale"; }  }

        public DateTime Created { get; set; }
    }
}
