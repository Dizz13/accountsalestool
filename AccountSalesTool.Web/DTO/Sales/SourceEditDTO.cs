﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountSalesTool.Web.Api.DTO.Sales
{
    public class SourceEditDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
