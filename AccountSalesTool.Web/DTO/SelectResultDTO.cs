﻿using System;

namespace AccountSalesTool.Web.Api.DTO
{
    public class SelectResultDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
