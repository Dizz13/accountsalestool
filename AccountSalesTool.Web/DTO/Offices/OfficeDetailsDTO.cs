﻿using System;

namespace AccountSalesTool.Web.Api.DTO.Offices
{
    public class OfficeDetailsDTO
    {
        public string Name { get; set; }
        public string User { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
    }
}
