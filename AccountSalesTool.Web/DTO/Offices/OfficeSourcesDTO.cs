﻿using System;
using System.Collections.Generic;

namespace AccountSalesTool.Web.Api.DTO.Offices
{
    public class OfficeSourcesDTO
    {
        public string Name { get; set; }
        public IEnumerable<Guid> Sources { get; set; }
    }
}
