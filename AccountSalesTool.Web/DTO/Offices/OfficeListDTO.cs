﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;

namespace AccountSalesTool.Web.Api.DTO.Offices
{
    public class OfficeListDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string User { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
    }
}
