﻿using System;

namespace AccountSalesTool.Web.Api.DTO.Offices
{
    public class OfficeEditDTO
    {
        public string Name { get; set; }
        public Guid? UserId { get; set; }
    }
}
