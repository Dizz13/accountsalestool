﻿using System;

namespace AccountSalesTool.Web.Api.DTO.Offices
{
    public class OfficeTickerDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Sales { get; set; }
    }
}
