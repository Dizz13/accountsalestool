﻿using System.Collections.Generic;

namespace AccountSalesTool.Web.Api.DTO
{
    public class BadRequestDTO
    {
        public List<string> Errors { get; set; } = new List<string>();

        public BadRequestDTO() { }
        public BadRequestDTO(string error) => Errors.Add(error);
    }
}
