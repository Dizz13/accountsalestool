﻿using AccountSalesTool.Core.Enums;
using System;

namespace AccountSalesTool.Web.Api.DTO.Users
{
    public class UserEditDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public UserTypeEnum Type { get; set; }
        public Guid? OfficeId { get; set; }
    }
}
