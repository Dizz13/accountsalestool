﻿using System;
using System.Buffers.Text;

namespace AccountSalesTool.Web.Api.DTO.Users
{
    public class UserListDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Office { get; set; }

        public DateTime Created { get; set; }
    }
}
