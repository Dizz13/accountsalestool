﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountSalesTool.Web.Api.Models.Sales
{
    public class AddSaleModel
    {
        public string Phone { get; set; }
        public string Email { get; set; }
        public string StateId { get; set; }
        public string SourceId { get; set; }
        public string OfficeId { get; set; }
    }
}
