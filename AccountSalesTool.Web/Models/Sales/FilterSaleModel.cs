﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountSalesTool.Web.Api.Models.Sales
{
    public class FilterSaleModel : QueryPageModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid? StateId { get; set; }
        public Guid? SourceId { get; set; }
        public Guid? OfficeId { get; set; }

        public string StrStartDate { get; set; }
        public string StrEndDate { get; set; }
        public int TimeZoneOffset { get; set; }
    }
}
