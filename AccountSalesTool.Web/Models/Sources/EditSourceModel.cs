﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountSalesTool.Web.Api.Models.Sources
{
    public class EditSourceModel
    {
        public string Name { get; set; }
    }
}
