﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountSalesTool.Web.Api.Models.Sources
{
    public class AddSourceModel
    {
        public string Name { get; set; }
    }
}
