﻿namespace AccountSalesTool.Web.Api.Models.Auth
{
    public class SignInModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
