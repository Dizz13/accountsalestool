﻿using System;

namespace AccountSalesTool.Web.Api.Models.Offices
{
    public class AddOfficeModel
    {
        public string Name { get; set; }
        public Guid? UserId { get; set; }
    }
}
