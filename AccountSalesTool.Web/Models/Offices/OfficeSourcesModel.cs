﻿using System;
using System.Collections.Generic;

namespace AccountSalesTool.Web.Api.Models.Offices
{
    public class OfficeSourcesModel
    {
        public IEnumerable<Guid> Sources { get; set; }
    }
}
