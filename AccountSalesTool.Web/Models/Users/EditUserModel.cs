﻿using AccountSalesTool.Core.Enums;
using System;

namespace AccountSalesTool.Web.Api.Models.Users
{
    public class EditUserModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public UserTypeEnum Type { get; set; }
        public Guid? OfficeId { get; set; }
    }
}
