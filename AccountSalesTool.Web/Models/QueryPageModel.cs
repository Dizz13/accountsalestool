﻿namespace AccountSalesTool.Web.Api.Models
{
    public class QueryPageModel
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public string OrderBy { get; set; }
        public bool IsAscending { get; set; }
    }
}
