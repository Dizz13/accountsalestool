﻿using Microsoft.AspNetCore.Authorization;

namespace AccountSalesTool.Web.Api.Auth
{
    public class Policies
    {
        public const string Admin = "Admin";
        public const string Office = "Office";
        public const string Report = "Report";

        public static AuthorizationPolicy AdminPolicy()
        {
            return new AuthorizationPolicyBuilder().RequireAuthenticatedUser().RequireRole(Admin).Build();
        }

        public static AuthorizationPolicy OfficePolicy()
        {
            return new AuthorizationPolicyBuilder().RequireAuthenticatedUser().RequireRole(Office).Build();
        }

        public static AuthorizationPolicy ReportPolicy()
        {
            return new AuthorizationPolicyBuilder().RequireAuthenticatedUser().RequireRole(Report).Build();
        }
    }
}
