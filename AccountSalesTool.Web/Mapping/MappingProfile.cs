﻿using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Pagination;
using AccountSalesTool.Data.Expressions;
using AccountSalesTool.Web.Api.DTO;
using AccountSalesTool.Web.Api.DTO.Offices;
using AccountSalesTool.Web.Api.DTO.Sales;
using AccountSalesTool.Web.Api.DTO.Sources;
using AccountSalesTool.Web.Api.DTO.Users;
using AccountSalesTool.Web.Api.Models;
using AccountSalesTool.Web.Api.Models.Offices;
using AccountSalesTool.Web.Api.Models.Sales;
using AccountSalesTool.Web.Api.Models.Sources;
using AccountSalesTool.Web.Api.Models.Users;
using AutoMapper;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace AccountSalesTool.Web.Api.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Domain to DTO
            CreateMap<IList<ValidationFailure>, BadRequestDTO>()
                .ForMember(dto => dto.Errors, opt => opt.MapFrom(errors => errors.Select(error => error.ErrorMessage)));

            CreateMap<User, UserListDTO>()
                .ForMember(dto => dto.Type, opt => opt.MapFrom(domain => domain.Type.ToString()))
                .ForMember(dto => dto.Office, opt => opt.MapFrom(domain => domain.Office != null ? domain.Office.Office.Name : "None"))
                .ForMember(dto => dto.Created, opt => opt.MapFrom(domain => domain.CreatedOn));
            CreateMap<User, UserEditDTO>()
                .ForMember(dto => dto.OfficeId, opt => opt.MapFrom(domain => domain.Office != null ? (Guid?)domain.Office.OfficeId : null));
            CreateMap<User, ChangePasswordDTO>();
            CreateMap<User, SelectResultDTO>();

            CreateMap<Sale, SaleListDTO>()
                .ForMember(dto => dto.Office, opt => opt.MapFrom(domain => domain.Office != null ? domain.Office.Name : null))
                .ForMember(dto => dto.Source, opt => opt.MapFrom(domain => domain.Source != null ? domain.Source.Name : null))
                .ForMember(dto => dto.State, opt => opt.MapFrom(domain => domain.State != null ? domain.State.Name : null))
                .ForMember(dto => dto.Created, opt => opt.MapFrom(domain => domain.CreatedOn));


            CreateMap<Source, SourceListDTO>()
                .ForMember(dto => dto.Created, opt => opt.MapFrom(domain => domain.CreatedOn));
            CreateMap<Source, SourceEditDTO>();
            CreateMap<QueryPageModel, IQueryPage<Source>>()
                .ForMember(domain => domain.Match, opt => opt.MapFrom(model => GetEmptyMatchLambda(new Source())))
                .ForMember(domain => domain.OrderBy, opt => opt.MapFrom(model => GetSourceSorting(model.OrderBy)));
            CreateMap<QueryPageModel, IQueryPage<Office>>()
                .ForMember(domain => domain.Match, opt => opt.MapFrom(model => GetEmptyMatchLambda(new Office())))
                .ForMember(domain => domain.OrderBy, opt => opt.MapFrom(model => GetOfficeSorting(model.OrderBy)));

            CreateMap<Office, OfficeListDTO>()
                .ForMember(dto => dto.User, opt => opt.MapFrom(domain => domain.User != null ? domain.User.User.Name : "None"))
                .ForMember(dto => dto.Created, opt => opt.MapFrom(domain => domain.CreatedOn))
                .ForMember(dto => dto.Updated, opt => opt.MapFrom(domain => domain.UpdatedOn));
            CreateMap<Office, OfficeDetailsDTO>()
                .ForMember(dto => dto.User, opt => opt.MapFrom(domain => domain.User != null ? domain.User.User.Name : "None"))
                .ForMember(dto => dto.Created, opt => opt.MapFrom(domain => domain.CreatedOn))
                .ForMember(dto => dto.Updated, opt => opt.MapFrom(domain => domain.UpdatedOn));
            CreateMap<Office, OfficeEditDTO>()
                .ForMember(dto => dto.UserId, opt => opt.MapFrom(domain => domain.User != null ? (Guid?)domain.User.UserId : null));
            CreateMap<Office, SelectResultDTO>();
            CreateMap<Office, OfficeSourcesDTO>()
                .ForMember(dto => dto.Sources, opt => opt.MapFrom(domain => domain.Sources.Select(s => s.SourceId)));
            CreateMap<Office, OfficeTickerDTO>()
                .ForMember(dto => dto.Sales, opt => opt.MapFrom(domain => domain.Sales.Count));

            CreateMap<Source, SelectResultDTO>();

            CreateMap<State, SelectResultDTO>();

            // Model to Domain
            CreateMap<AddUserModel, User>()
                .ForMember(domain => domain.PasswordHash, opt => opt.MapFrom(model => model.Password));
            CreateMap<EditUserModel, User>();
            CreateMap<ChangePasswordModel, User>()
                .ForMember(domain => domain.PasswordHash, opt => opt.MapFrom(model => model.Password));

            CreateMap<QueryPageModel, IQueryPage<User>>()
                .ForMember(domain => domain.Match, opt => opt.MapFrom(model => GetEmptyMatchLambda(new User())))
                .ForMember(domain => domain.OrderBy, opt => opt.MapFrom(model => GetUserSorting(model.OrderBy)));

            CreateMap<FilterSaleModel, IQueryPage<Sale>>()
                .ForMember(domain => domain.Match, opt => opt.MapFrom(model => GetSaleMatchLambda(model)))
                .ForMember(domain => domain.OrderBy, opt => opt.MapFrom(model => GetSaleSorting(model.OrderBy)));

            CreateMap<AddOfficeModel, Office>();
            CreateMap<EditOfficeModel, Office>();
            CreateMap<OfficeSourcesModel, Office>()
                .ForMember(domain => domain.Sources, opt => opt.MapFrom(model => model.Sources.Select(s => new OfficeSource { SourceId = s })));

            CreateMap<AddSaleModel, Sale>();

            CreateMap<AddSourceModel, Source>();
            CreateMap<EditSourceModel, Source>();
        }

        private Expression<Func<Office, object>> GetOfficeSorting(string orderBy)
        {
            switch (orderBy)
            {
                case "created":
                    return office => office.CreatedOn;
                case "name":
                    return office => office.Name;
                case "user":
                    return office => office.User;
                case "updated":
                    return office => office.UpdatedOn;
                default:
                    return GetOrderByLambda(new Office(), orderBy);
            }
        }

        private Expression<Func<User, object>> GetUserSorting(string orderBy)
        {
            switch (orderBy)
            {
                case "created":
                    return user => user.CreatedOn;
                case "type":
                    return user => user.Type;
                case "office":
                    return user => user.Office != null ? user.Office.Office.Name : "";
                default:
                    return GetOrderByLambda(new User(), orderBy);
            }
        }

        private Expression<Func<Sale, object>> GetSaleSorting(string orderBy)
        {
            switch (orderBy)
            {
                case "created":
                    return sale => sale.CreatedOn;
                case "phone":
                    return sale => sale.Phone;

                case "office":
                    return sale => sale.Office != null ? sale.Office.Name : "";

                case "source":
                    return sale => sale.Source != null ? sale.Source.Name : "";

                case "state":
                    return sale => sale.State != null ? sale.State.Name : "";

                default:
                    return GetOrderByLambda(new Sale(), orderBy);
            }
        }

        private Expression<Func<Source, object>> GetSourceSorting(string orderBy)
        {
            switch (orderBy)
            {
                case "created":
                    return source => source.CreatedOn;
                case "name":
                    return source => source.Name;
                default:
                    return GetOrderByLambda(new Source(), orderBy);
            }
        }

        private Expression<Func<TEntity, bool>> GetEmptyMatchLambda<TEntity>(TEntity entity)
        {
            var parameterExpression = Expression.Parameter(entity.GetType(), "entity");
            var constant = Expression.Constant(true);
            var lambda = Expression.Lambda<Func<TEntity, bool>>(constant, parameterExpression);
            return lambda;
        }

        private Expression<Func<TEntity, bool>> GetMatchLambda<TEntity>(TEntity entity, string propertyName, object propertyValue)
        {
            var parameterExpression = Expression.Parameter(entity.GetType(), "entity");
            var constant = Expression.Constant(propertyValue);
            var property = Expression.Property(parameterExpression, propertyName);
            var expression = Expression.Equal(property, constant);
            var lambda = Expression.Lambda<Func<TEntity, bool>>(expression, parameterExpression);
            return lambda;
        }

        private Expression<Func<Sale, bool>> GetSaleMatchLambda(FilterSaleModel model)
        {
            Expression<Func<Sale, bool>> lambda = sale => sale.CreatedOn > model.StartDate && sale.CreatedOn < model.EndDate;
            if (model.OfficeId.HasValue)
            {
                lambda = lambda.And(sale => sale.OfficeId == model.OfficeId);
            }

            if (model.SourceId.HasValue)
            {
                lambda = lambda.And(sale => sale.SourceId == model.SourceId);
            }

            if (model.StateId.HasValue)
            {
                lambda = lambda.And(sale => sale.StateId == model.StateId);
            }

            return lambda;
        }

        private Expression<Func<TEntity, object>> GetOrderByLambda<TEntity>(TEntity entity, string propertyName)
        {
            var parameterExpression = Expression.Parameter(entity.GetType(), "entity");
            var property = Expression.Property(parameterExpression, propertyName);
            var lambda = Expression.Lambda<Func<TEntity, object>>(property, parameterExpression);
            return lambda;
        }
    }
}
