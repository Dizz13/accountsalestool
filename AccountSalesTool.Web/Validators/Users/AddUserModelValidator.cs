﻿using AccountSalesTool.Core.Services;
using AccountSalesTool.Web.Api.Models.Users;
using FluentValidation;

namespace AccountSalesTool.Web.Api.Validators.Users
{
    public class AddUserModelValidator : AbstractValidator<AddUserModel>
    {
        private readonly IUserService _userService;

        public AddUserModelValidator(IUserService userService)
        {
            _userService = userService;

            CascadeMode = CascadeMode.Stop;

            RuleFor(a => a.Email)
                .NotEmpty().WithMessage("Email address can't be empty")
                .EmailAddress().WithMessage("Invalid Email address")
                .MustAsync(async (email, cancellation) => {
                    var exists = await _userService.IsExistsAsync(email);
                    return !exists;
                }).WithMessage("Email address must be unique");

            RuleFor(a => a.Name)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(a => a.Password)
                .NotEmpty()
                .MinimumLength(8);
        }
    }
}
