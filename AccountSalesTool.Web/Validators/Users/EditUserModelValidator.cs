﻿using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Services;
using AccountSalesTool.Web.Api.Models.Users;
using FluentValidation;
using System;

namespace AccountSalesTool.Web.Api.Validators.Users
{
    public class EditUserModelValidator : AbstractValidator<EditUserModel>
    {
        private readonly IUserService _userService;
        private readonly User _user;
        private readonly Guid _currentUserId;

        public EditUserModelValidator(IUserService userService, User user, Guid currentUserId)
        {
            _userService = userService;
            _user = user;
            _currentUserId = currentUserId;

            CascadeMode = CascadeMode.Stop;

            RuleFor(a => a.Email)
                .NotEmpty().WithMessage("Email address can't be empty")
                .EmailAddress().WithMessage("Invalid Email address")
                .MustAsync(async (email, cancellation) => {
                    if (email != _user.Email)
                    {
                        var exists = await _userService.IsExistsAsync(email);
                        return !exists;
                    }
                    return true;
                }).WithMessage("Email address must be unique");

            RuleFor(a => a.Name)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(a => a.Type)
                .Must((type) => {
                    if (user.Id == _currentUserId)
                        return type == user.Type;
                    return true;
                }).WithMessage("You can not modify your own type");
        }
    }
}
