﻿using FluentValidation;
using System;

namespace AccountSalesTool.Web.Api.Validators.Users
{
    public class DeleteUserValidator : AbstractValidator<Guid>
    {
        private readonly Guid _currentUserId;

        public DeleteUserValidator(Guid currentUserId)
        {
            _currentUserId = currentUserId;

            CascadeMode = CascadeMode.Stop;

            RuleFor(a => a)
                .NotEqual(_currentUserId).WithMessage("You can not delete yourself");
        }
    }
}
