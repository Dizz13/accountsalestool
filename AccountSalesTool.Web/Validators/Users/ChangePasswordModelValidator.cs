﻿using AccountSalesTool.Web.Api.Models.Users;
using FluentValidation;

namespace AccountSalesTool.Web.Api.Validators.Users
{
    public class ChangePasswordModelValidator : AbstractValidator<ChangePasswordModel>
    {
        public ChangePasswordModelValidator()
        {
            CascadeMode = CascadeMode.Stop;

            RuleFor(a => a.Password)
                .NotEmpty()
                .MinimumLength(8);
        }
    }
}
