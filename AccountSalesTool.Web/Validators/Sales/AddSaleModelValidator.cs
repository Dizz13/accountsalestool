﻿using AccountSalesTool.Core.Services;
using AccountSalesTool.Web.Api.Models.Sales;
using AccountSalesTool.Web.Api.Models.Users;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountSalesTool.Web.Api.Validators.Sales
{
    public class AddSaleModelValidator : AbstractValidator<AddSaleModel>
    {
        private readonly ISaleService _saleService;

        public AddSaleModelValidator(ISaleService saleService)
        {
            _saleService = saleService;

            CascadeMode = CascadeMode.Stop;

            RuleFor(a => a.Email)
                .NotEmpty().WithMessage("Email address can't be empty")
                .EmailAddress().WithMessage("Invalid Email address");

            RuleFor(a => a.Phone)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(a => a.SourceId)
                .NotEmpty();

            RuleFor(a => a.StateId)
                .NotEmpty();

            RuleFor(a => a.OfficeId)
                .NotEmpty();
        }
    }
}
