﻿using AccountSalesTool.Core.Services;
using AccountSalesTool.Web.Api.Models.Sources;
using FluentValidation;

namespace AccountSalesTool.Web.Api.Validators.Sources
{
    public class EditSourceModelValidator : AbstractValidator<EditSourceModel>
    {
        private readonly ISourceService _sourceService;

        public EditSourceModelValidator(ISourceService sourceService)
        {
            _sourceService = sourceService;
            CascadeMode = CascadeMode.Stop;
            RuleFor(a => a.Name)
                .NotEmpty()
                .MaximumLength(50)
                .MustAsync(async (name, cancellation) => {
                    var exists = await sourceService.IsExistsAsync(name);
                    return !exists;
                }).WithMessage("Source must be unique");
        }
    }
}
