﻿using AccountSalesTool.Core.Services;
using AccountSalesTool.Web.Api.Models.Sources;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountSalesTool.Web.Api.Validators.Sources
{
    public class AddSourceModelValidator : AbstractValidator<AddSourceModel>
    {
        private readonly ISourceService _sourceService;

        public AddSourceModelValidator(ISourceService sourceService)
        {
            _sourceService = sourceService;
            CascadeMode = CascadeMode.Stop;
            RuleFor(a => a.Name)
                .NotEmpty()
                .MaximumLength(50)
                .MustAsync(async (name, cancellation) => {
                    var exists = await sourceService.IsExistsAsync(name);
                        return !exists;
                }).WithMessage("Source must be unique"); 
        }
    }
}
