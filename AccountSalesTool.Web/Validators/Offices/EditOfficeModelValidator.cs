﻿using AccountSalesTool.Core.Services;
using AccountSalesTool.Web.Api.Models.Offices;
using FluentValidation;

namespace AccountSalesTool.Web.Api.Validators.Offices
{
    public class EditOfficeModelValidator : AbstractValidator<EditOfficeModel>
    {
        private readonly IOfficeService _officeService;
        public EditOfficeModelValidator(IOfficeService officeService)
        {
            _officeService = officeService;
            CascadeMode = CascadeMode.Stop;

            RuleFor(a => a.Name)
                .NotEmpty()
                .MaximumLength(7)
                .MustAsync(async (name, cancellation) => {
                    var exists = await _officeService.IsExistsAsync(name);
                    return !exists;
                }).WithMessage("Office name must be unique");
        }
    }
}
