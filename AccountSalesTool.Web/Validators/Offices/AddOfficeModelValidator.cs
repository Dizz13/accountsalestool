﻿using AccountSalesTool.Core.Services;
using AccountSalesTool.Web.Api.Models.Offices;
using FluentValidation;

namespace AccountSalesTool.Web.Api.Validators.Offices
{
    public class AddOfficeModelValidator : AbstractValidator<AddOfficeModel>
    {
        private readonly IUserService _userService;
        private readonly IOfficeService _officeService;

        public AddOfficeModelValidator(IUserService userService, IOfficeService officeService)
        {
            _userService = userService;
            _officeService = officeService;

            CascadeMode = CascadeMode.Stop;

            RuleFor(a => a.UserId)
                .MustAsync(async (userId, cancellation) => {
                    if(userId.HasValue)
                        return await _userService.IsAvailableForOfficeAsync(userId.Value);
                    return true;
                }).WithMessage("This user is not available for assign");

            RuleFor(a => a.Name)
                .NotEmpty()
                .MaximumLength(7)
                .MustAsync(async (name, cancellation) => {
                    var exists = await _officeService.IsExistsAsync(name);
                    return !exists;
                }).WithMessage("Office name must be unique");
        }
    }
}
