@echo off

git pull

echo ################
echo ### Stop IIS ###
echo ################
iisreset /stop
echo ####################
echo ### IIS Stopped ####
echo ####################

echo ####################
echo ### Remove files ###
echo ####################
pushd AccountSalesTool.Client
rmdir /S /Q dist
popd

pushd AccountSalesTool.Client 
rmdir /S /Q node_modules 
popd

pushd AccountSalesTool.Data 
rmdir /S /Q bin 
popd

pushd AccountSalesTool.Data 
rmdir /S /Q obj 
popd

pushd AccountSalesTool.Web 
rmdir /S /Q bin 
popd

pushd AccountSalesTool.Web 
rmdir /S /Q obj 
popd

pushd c:\inetpub\wwwroot
rmdir /S /Q accountsalestool
popd
echo ######################
echo ### Files removed ####
echo ######################

echo ######################
echo ### Dotnet restore ###
echo ######################
dotnet restore
echo ##########################
echo ### Dotnet restore end ###
echo ##########################

echo #####################################
echo ### Set aspnetcore_environment=qa ###
echo #####################################
setx aspnetcore_environment qa /M
echo #########################################
echo ### Set aspnetcore_environment=qa end ###
echo #########################################

echo ##########################
echo ### Dotnet publish Web ###
echo ##########################
dotnet publish --output c:\inetpub\wwwroot\accountsalestool\
echo ##############################
echo ### Dotnet publish Web End ###
echo ##############################

echo #################
echo ### DB update ###
echo #################
dotnet ef database update --project AccountSalesTool.Data --startup-project AccountSalesTool.Web
echo #####################
echo ### DB update end ###
echo #####################

echo ###################
echo ### npm install ###
echo ###################
cd AccountSalesTool.Client 
call npm install 
cd ..
echo #######################
echo ### npm install end ###
echo #######################

echo ####################
echo ### client build ###
echo ####################
pushd AccountSalesTool.Client 
call npm run buildqa
popd
echo ########################
echo ### client build end ###
echo ########################

echo #################
echo ### iis start ###
echo #################
iisreset /start
echo ###################
echo ### iis started ###
echo ###################
pause