﻿using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Pagination;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AccountSalesTool.Core.Services
{
    public interface ISaleService
    {
        Task<IEnumerable<Sale>> GetAllAsync();
        Task<long> CountAsync();
        Task<long> GetSalesCountByOfficeAsync(Guid officeId, DateTime from, DateTime to);
        Task<Guid> CreateAsync(Sale sale);

        Task<IPage<Sale>> GetPageAsync(IQueryPage<Sale> query);
    }
}
