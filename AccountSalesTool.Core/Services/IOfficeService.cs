﻿using AccountSalesTool.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AccountSalesTool.Core.Pagination;

namespace AccountSalesTool.Core.Services
{
    public interface IOfficeService
    {
        Task<IEnumerable<Office>> GetAllAsync();
        Task<Office> GetByIdAsync(Guid id);
        Task<long> CountAsync();
        Task<Guid> CreateAsync(Office office);
        Task<Guid> UpdateAsync(Office office);
        Task<bool> DeleteAsync(Guid id);
        Task<IEnumerable<Office>> GetNotAssignedUserOffices(Guid? currentAssignedId);
        Task<IPage<Office>> GetPageAsync(IQueryPage<Office> query);

        Task<bool> IsExistsAsync(string name);
    }
}
