﻿using System;
using System.Threading.Tasks;

namespace AccountSalesTool.Core.Services
{
    public interface IOfficeUserService
    {
        Task CreateRelationshipAsync(Guid officeId, Guid userId);
        Task<bool> DeleteRelationshipByOfficeAsync(Guid officeId);
        Task<bool> DeleteRelationshipByUserAsync(Guid userId);
    }
}
