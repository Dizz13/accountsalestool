﻿using AccountSalesTool.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AccountSalesTool.Core.Pagination;

namespace AccountSalesTool.Core.Services
{
    public interface ISourceService
    {
        Task<IEnumerable<Source>> GetAllAsync();
        Task<long> CountAsync();
        Task<IEnumerable<Source>> GetByOffice(Guid officeId);
        Task<Guid> CreateAsync(Source source);
        Task<Source> GetByIdAsync(Guid id);
        Task<Guid> UpdateAsync(Source source);
        Task<bool> DeleteAsync(Guid id);
        Task<IPage<Source>> GetPageAsync(IQueryPage<Source> query);
        Task<bool> IsExistsAsync(string email);
    }
}
