﻿using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Pagination;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AccountSalesTool.Core.Services
{
    public interface IUserService
    {
        Task<User> GetByIdAsync(Guid id);
        Task<User> GetByEmailAndPasswordAsync(string email, string password);
        Task<IEnumerable<User>> GetAllAsync();
        Task<bool> IsExistsAsync(string email);
        Task<Guid> CreateAsync(User user);
        Task<Guid> UpdateAsync(User user, bool calculateHash);
        Task<bool> DeleteAsync(Guid id);
        Task<long> CountAsync();
        Task<IEnumerable<User>> GetNotAssignedOfficeUsers(Guid? currentAssignedId);
        Task<bool> IsAvailableForOfficeAsync(Guid id);
        Task<IPage<User>> GetPageAsync(IQueryPage<User> query);
    }
}
