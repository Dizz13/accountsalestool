﻿using AccountSalesTool.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AccountSalesTool.Core.Services
{
    public interface IStateService
    {
        Task<IEnumerable<State>> GetAllAsync();

        Task<State> GetByIdAsync(Guid id);
    }
}
