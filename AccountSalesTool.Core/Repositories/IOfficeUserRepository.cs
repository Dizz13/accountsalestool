﻿using AccountSalesTool.Core.Models;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AccountSalesTool.Core.Repositories
{
    public interface IOfficeUserRepository
    {
        Task AddAsync(OfficeUser entity);
        Task<OfficeUser> SingleOrDefaultAsync(Expression<Func<OfficeUser, bool>> predicate);
        void Remove(OfficeUser entity);
    }
}
