﻿using AccountSalesTool.Core.Models;

namespace AccountSalesTool.Core.Repositories
{
    public interface ISaleRepository : IRepository<Sale>
    {
    }
}
