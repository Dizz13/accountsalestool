﻿using AccountSalesTool.Core.Models;

namespace AccountSalesTool.Core.Repositories
{
    public interface ISourceRepository : IRepository<Source>
    {
    }
}
