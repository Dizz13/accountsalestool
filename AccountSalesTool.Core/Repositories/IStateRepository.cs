﻿using AccountSalesTool.Core.Models;

namespace AccountSalesTool.Core.Repositories
{
    public interface IStateRepository : IRepository<State>
    {
    }
}
