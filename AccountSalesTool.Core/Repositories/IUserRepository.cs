﻿using AccountSalesTool.Core.Models;

namespace AccountSalesTool.Core.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
    }
}
