﻿using AccountSalesTool.Core.Models;

namespace AccountSalesTool.Core.Repositories
{
    public interface IOfficeRepository : IRepository<Office>
    {
    }
}
