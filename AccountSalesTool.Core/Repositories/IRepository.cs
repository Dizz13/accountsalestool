﻿using AccountSalesTool.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AccountSalesTool.Core.Repositories
{
    public interface IRepository<TEntity> where TEntity : EntityBase, new()
    {
        ValueTask<TEntity> GetByIdAsync(Guid id);
        Task<IEnumerable<TEntity>> GetAllAsync();
        Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);
        Task AddAsync(TEntity entity);
        Task AddRangeAsync(IEnumerable<TEntity> entities);
        void Update(TEntity entity);
        void UpdateRange(IEnumerable<TEntity> entities);
        void Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entities);
        Task<int> CountAllAsync();
        Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate);
        Task<IEnumerable<TEntity>> FindAllAsync(Expression<Func<TEntity, bool>> match);
        Task<IEnumerable<TEntity>> FindAllAsync(Expression<Func<TEntity, bool>> match, int take);
        Task<IEnumerable<TEntity>> FindAllAsync(Expression<Func<TEntity, bool>> match, int take, int skip);
        Task<IEnumerable<TEntity>> FindAllOrderedAsync(Expression<Func<TEntity, bool>> match, Expression<Func<TEntity, object>> orderBy, bool isAscending);
        Task<IEnumerable<TEntity>> FindAllOrderedAsync(Expression<Func<TEntity, bool>> match, Expression<Func<TEntity, object>> orderBy, bool isAscending, int take);
        Task<IEnumerable<TEntity>> FindAllOrderedAsync(Expression<Func<TEntity, bool>> match, Expression<Func<TEntity, object>> orderBy, bool isAscending, int take, int skip);
    }
}
