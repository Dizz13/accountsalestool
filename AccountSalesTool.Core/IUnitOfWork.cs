﻿using AccountSalesTool.Core.Repositories;
using System;
using System.Threading.Tasks;

namespace AccountSalesTool.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IOfficeRepository Offices { get; }
        IOfficeUserRepository OfficeUserRelationships { get; }
        ISaleRepository Sales { get; }
        ISourceRepository Sources { get; }
        IStateRepository States { get; }
        IUserRepository Users { get; }
        Task<int> CommitAsync();
    }
}
