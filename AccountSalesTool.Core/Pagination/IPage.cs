﻿using System.Collections.Generic;

namespace AccountSalesTool.Core.Pagination
{
    public interface IPage<TEntity>
    {
        public IEnumerable<TEntity> Data { get; }
        public int Number { get; }
        public int Size { get; }
        public int Total { get; }
    }
}
