﻿using AccountSalesTool.Core.Models;
using System;
using System.Linq.Expressions;

namespace AccountSalesTool.Core.Pagination
{
    public interface IQueryPage<TEntity> where TEntity : EntityBase, new()
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public Expression<Func<TEntity, bool>> Match { get; set; }
        public Expression<Func<TEntity, object>> OrderBy { get; set; }
        public bool IsAscending { get; set; }
    }
}
