﻿using System;

namespace AccountSalesTool.Core.Models
{
    public class OfficeSource
    {
        public Guid OfficeId { get; set; }
        public Guid SourceId { get; set; }

        public virtual Office Office { get; set; }
        public virtual Source Source { get; set; }
    }
}
