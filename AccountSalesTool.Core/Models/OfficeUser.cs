﻿using System;

namespace AccountSalesTool.Core.Models
{
    public class OfficeUser
    {
        public Guid OfficeId { get; set; }
        public Guid UserId { get; set; }

        public virtual Office Office { get; set; }
        public virtual User User { get; set; }
    }
}
