﻿using System;

namespace AccountSalesTool.Core.Models
{
    public class Sale : EntityBase
    {
        public string Phone { get; set; }
        public string Email { get; set; }
        public Guid StateId { get; set; }
        public Guid SourceId { get; set; }
        public Guid OfficeId { get; set; }

        public virtual State State { get; set; }
        public virtual Source Source { get; set; }
        public virtual Office Office { get; set; }
    }
}
