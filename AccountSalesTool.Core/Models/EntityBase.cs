﻿using System;

namespace AccountSalesTool.Core.Models
{
    public abstract class EntityBase
    {
        public Guid Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public byte[] Timestamp { get; set; }
    }
}
