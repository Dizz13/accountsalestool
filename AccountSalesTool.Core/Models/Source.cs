﻿using System.Collections.Generic;

namespace AccountSalesTool.Core.Models
{
    public class Source : EntityBase
    {
        public string Name { get; set; }

        public virtual ICollection<OfficeSource> Offices { get; set; }
        public virtual ICollection<Sale> Sales { get; set; }

        public Source()
        {
            Offices = new List<OfficeSource>();
            Sales = new List<Sale>();
        }
    }
}
