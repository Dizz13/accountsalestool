﻿using System.Collections.Generic;

namespace AccountSalesTool.Core.Models
{
    public class Office : EntityBase
    {
        public string Name { get; set; }

        public virtual OfficeUser User { get; set; }
        public virtual ICollection<OfficeSource> Sources { get; set; }
        public virtual ICollection<Sale> Sales { get; set; }

        public Office()
        {
            Sources = new List<OfficeSource>();
            Sales = new List<Sale>();
        }
    }
}
