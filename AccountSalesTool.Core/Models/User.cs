﻿using AccountSalesTool.Core.Enums;

namespace AccountSalesTool.Core.Models
{
    public class User : EntityBase
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string PasswordHash { get; set; }
        public UserTypeEnum Type { get; set; }

        public virtual OfficeUser Office { get; set; }
    }
}
