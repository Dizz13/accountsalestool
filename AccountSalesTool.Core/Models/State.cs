﻿using System.Collections.Generic;

namespace AccountSalesTool.Core.Models
{
    public class State : EntityBase
    {
        public string Name { get; set; }
        public string Code { get; set; }

        public virtual ICollection<Sale> Sales { get; set; }

        public State() => Sales = new List<Sale>();
    }
}
