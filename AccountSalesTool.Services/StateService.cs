﻿using AccountSalesTool.Core;
using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Services;
using NLog;

namespace AccountSalesTool.Services
{
    public class StateService : Service<State>, IStateService
    {
        public StateService(IUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.States, LogManager.GetCurrentClassLogger())
        {
        }
    }
}
