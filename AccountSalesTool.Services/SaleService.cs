﻿using AccountSalesTool.Core;
using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Services;
using NLog;
using System;
using System.Threading.Tasks;

namespace AccountSalesTool.Services
{
    public class SaleService : Service<Sale>, ISaleService
    {
        public SaleService(IUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.Sales, LogManager.GetCurrentClassLogger()) { }

        public async Task<long> GetSalesCountByOfficeAsync(Guid officeId, DateTime from, DateTime to) 
            => await UnitOfWork.Sales.CountAsync(s => s.OfficeId == officeId && s.CreatedOn > from && s.CreatedOn < to);

        public override async Task<Guid> CreateAsync(Sale sale)
        {
            return await base.CreateAsync(sale);
        }
    }
}
