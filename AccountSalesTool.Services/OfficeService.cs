﻿using AccountSalesTool.Core;
using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Services;
using NLog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AccountSalesTool.Services
{
    public class OfficeService : Service<Office>, IOfficeService
    {
        public OfficeService(IUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.Offices, LogManager.GetCurrentClassLogger()) { }

        public async Task<IEnumerable<Office>> GetNotAssignedUserOffices(Guid? currentAssignedId)
        {
            IEnumerable<Office> result;
            if (currentAssignedId.HasValue)
                result = await UnitOfWork.Offices.FindAsync(a => a.User == null || a.User.UserId == currentAssignedId);
            else
                result = await UnitOfWork.Offices.FindAsync(a => a.User == null);
            return result;
        }

        public async Task<bool> IsExistsAsync(string name) => await UnitOfWork.Offices.CountAsync(a => a.Name == name) > 0;

    }
}
