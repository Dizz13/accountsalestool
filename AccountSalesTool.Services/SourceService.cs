﻿using AccountSalesTool.Core;
using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Services;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AccountSalesTool.Services
{
    public class SourceService : Service<Source>, ISourceService
    {
        public SourceService(IUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.Sources, LogManager.GetCurrentClassLogger()) { }

        public async Task<IEnumerable<Source>> GetByOffice(Guid officeId)
        {
            var office = await UnitOfWork.Offices.GetByIdAsync(officeId);
            if (office == null)
                return new List<Source>();
            var result = office.Sources.Select(os => os.Source);
            return result;
        }

        public async Task<bool> IsExistsAsync(string name) => await UnitOfWork.Sources.CountAsync(a => a.Name == name) > 0;
    }
}
