﻿using AccountSalesTool.Core;
using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Repositories;
using AccountSalesTool.Core.Services;
using NLog;
using System;
using System.Threading.Tasks;

namespace AccountSalesTool.Services
{
    public class OfficeUserService : IOfficeUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IOfficeUserRepository _repository;
        private readonly ILogger _logger;

        public OfficeUserService(IUnitOfWork unitOfWork) 
        {
            _unitOfWork = unitOfWork;
            _repository = _unitOfWork.OfficeUserRelationships;
            _logger = LogManager.GetCurrentClassLogger();
        }

        public async Task CreateRelationshipAsync(Guid officeId, Guid userId)
        {
            var relationship = new OfficeUser()
            {
                OfficeId = officeId,
                UserId = userId
            };
            await _repository.AddAsync(relationship);
            await _unitOfWork.CommitAsync();
        }

        public async Task<bool> DeleteRelationshipByOfficeAsync(Guid officeId)
        {
            var relationship = await _repository.SingleOrDefaultAsync(a => a.OfficeId == officeId);
            return await DeleteAsync(relationship);
        }

        public async Task<bool> DeleteRelationshipByUserAsync(Guid userId)
        {
            var relationship = await _repository.SingleOrDefaultAsync(a => a.UserId == userId);
            return await DeleteAsync(relationship);
        }

        private async Task<bool> DeleteAsync(OfficeUser relationship)
        {
            if (relationship == null)
                return false;
            _repository.Remove(relationship);
            await _unitOfWork.CommitAsync();
            return true;
        }
    }
}
