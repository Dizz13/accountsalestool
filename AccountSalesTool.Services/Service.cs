﻿using AccountSalesTool.Core;
using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Pagination;
using AccountSalesTool.Core.Repositories;
using NLog;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AccountSalesTool.Services
{
    public abstract class Service<TEntity> where TEntity : EntityBase, new()
    {
        private readonly IRepository<TEntity> _repository;

        protected readonly IUnitOfWork UnitOfWork;
        protected readonly ILogger Logger;

        public Service(IUnitOfWork unitOfWork, IRepository<TEntity> repository, ILogger logger)
        {
            UnitOfWork = unitOfWork;
            _repository = repository;
            Logger = logger;
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync() => await _repository.GetAllAsync();
        public async Task<TEntity> GetByIdAsync(Guid id) => await _repository.GetByIdAsync(id);
        public async Task<long> CountAsync() => await _repository.CountAllAsync();

        public virtual async Task<Guid> CreateAsync(TEntity entity)
        {
            if (entity.Id == Guid.Empty)
                entity.Id = Guid.NewGuid();
            entity.CreatedOn = DateTime.UtcNow;
            entity.UpdatedOn = DateTime.UtcNow;
            await _repository.AddAsync(entity);
            await UnitOfWork.CommitAsync();
            return entity.Id;
        }

        public async Task<Guid> UpdateAsync(TEntity entity)
        {
            entity.UpdatedOn = DateTime.UtcNow;
            _repository.Update(entity);
            await UnitOfWork.CommitAsync();
            return entity.Id;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var entity = await _repository.GetByIdAsync(id);
            if (entity == null)
                return false;
            _repository.Remove(entity);
            await UnitOfWork.CommitAsync();
            return true;
        }

        public async Task<IPage<TEntity>> GetPageAsync(IQueryPage<TEntity> query)
        {
            var data = await _repository.FindAllOrderedAsync(query.Match, query.OrderBy, query.IsAscending, query.PageSize, (query.PageNumber - 1) * query.PageSize);
            var total = await _repository.CountAsync(query.Match);
            var result = new Page<TEntity>(data, query.PageNumber, query.PageSize, total);
            return result;
        }
    }
}
