﻿using AccountSalesTool.Core.Pagination;
using System.Collections.Generic;

namespace AccountSalesTool.Services
{
    public class Page<TEntity> : IPage<TEntity>
    {
        public Page(IEnumerable<TEntity> data, int number, int size, int total)
        {
            Data = data;
            Number = number;
            Size = size;
            Total = total;
        }

        public IEnumerable<TEntity> Data { get; }
        public int Number { get; }
        public int Size { get; }
        public int Total { get; }
    }
}
