﻿using AccountSalesTool.Core;
using AccountSalesTool.Core.Enums;
using AccountSalesTool.Core.Models;
using AccountSalesTool.Core.Pagination;
using AccountSalesTool.Core.Services;
using NLog;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AccountSalesTool.Services
{
    public class UserService : Service<User>, IUserService
    {
        public UserService(IUnitOfWork unitOfWork) : base(unitOfWork, unitOfWork.Users, LogManager.GetCurrentClassLogger()) { }

        public override async Task<Guid> CreateAsync(User user)
        {
            user.Id = Guid.NewGuid();
            user.PasswordHash = GetPasswordHash(user.Id, user.PasswordHash);
            return await base.CreateAsync(user);
        }

        public async Task<Guid> UpdateAsync(User user, bool calculateHash)
        {
            if(calculateHash)
                user.PasswordHash = GetPasswordHash(user.Id, user.PasswordHash);
            return await UpdateAsync(user);
        }

        public async Task<User> GetByEmailAndPasswordAsync(string email, string password)
        {
            Logger.Info("Test log");

            var user = await UnitOfWork.Users.SingleOrDefaultAsync(a => a.Email == email);
            if (user == null)
                return null;
            var passwordHash = GetPasswordHash(user.Id, password);
            if (user.PasswordHash != passwordHash)
                return null;
            return user;
        }

        public async Task<bool> IsExistsAsync(string email) => await UnitOfWork.Users.CountAsync(a => a.Email == email) > 0;

        public async Task<IEnumerable<User>> GetNotAssignedOfficeUsers(Guid? currentAssignedId)
        {
            IEnumerable<User> result;
            if (currentAssignedId.HasValue)
                result = await UnitOfWork.Users.FindAsync(a => a.Type == UserTypeEnum.Office && (a.Office == null || a.Office.OfficeId == currentAssignedId));
            else
                result = await UnitOfWork.Users.FindAsync(a => a.Type == UserTypeEnum.Office && a.Office == null);
            return result;
        }

        public async Task<bool> IsAvailableForOfficeAsync(Guid id)
        {
            var user = await UnitOfWork.Users.GetByIdAsync(id);
            if (user == null)
                return false;
            return user.Office == null;
        }

        private string GetPasswordHash(Guid id, string password)
        {
            var salt = Encoding.UTF8.GetBytes(id.ToString());
            var value = Encoding.UTF8.GetBytes(password);
            var hmac = new HMACMD5(salt);
            return Convert.ToBase64String(hmac.ComputeHash(value));
        }
    }
}
