import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { AdminGuard } from '../auth/guards/admin.guard';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AdminGuard],
    children: [
      {
        path: '',
        canActivateChild: [AdminGuard],
        children: [
          {
            path: 'sales',
            loadChildren: () => import('./sales/sales.module').then(m => m.SalesModule),
            canLoad: [AdminGuard]
          },
          {
            path: 'offices',
            loadChildren: () => import('./offices/offices.module').then(m => m.OfficesModule),
            canLoad: [AdminGuard]
          },
          {
            path: 'sources',
            loadChildren: () => import('./sources/sources.module').then(m => m.SourcesModule),
            canLoad: [AdminGuard]
          },
          {
            path: 'users',
            loadChildren: () => import('./users/users.module').then(m => m.UsersModule),
            canLoad: [AdminGuard]
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
