import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SourcesComponent } from './sources/sources.component';
import { AdminGuard } from 'src/app/auth/guards/admin.guard';
import { EditSourceResolverService } from 'src/app/resolvers/sources/edit-source-resolver.service';
import { AddSourceComponent } from './add-source/add-source.component';
import { EditSourceComponent } from './edit-source/edit-source.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AdminGuard],
    children: [
      {
        path: '',
        canActivateChild: [AdminGuard],
        children: [
          { path: '', component: SourcesComponent },
          { path: 'add', component: AddSourceComponent },
          {
            path: ':id',
            component: EditSourceComponent,
            resolve: {
              source: EditSourceResolverService
            }
          },
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SourcesRoutingModule { }
