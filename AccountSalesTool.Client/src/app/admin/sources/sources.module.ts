import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SourcesRoutingModule } from './sources-routing.module';
import { SourcesComponent } from './sources/sources.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/material-module';
import { AddSourceComponent } from './add-source/add-source.component';
import { EditSourceComponent } from './edit-source/edit-source.component';


@NgModule({
  declarations: [SourcesComponent, AddSourceComponent, EditSourceComponent],
  imports: [
    CommonModule,
    SourcesRoutingModule,
    ReactiveFormsModule,
    MaterialModule
  ]
})
export class SourcesModule { }