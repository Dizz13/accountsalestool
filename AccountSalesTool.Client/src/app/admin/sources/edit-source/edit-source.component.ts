import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';

import { environment } from 'src/environments/environment';

import { IBadRequest } from 'src/app/interfaces/bad-request';
import { ISourceEdit } from 'src/app/interfaces/sources/source-edit';
import { EditSourceModel } from 'src/app/models/sources/edit-source-model';
import { SourceService } from 'src/app/services/source.service';
import { ConfirmDialogComponent } from 'src/app/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'ast-edit-source',
  templateUrl: './edit-source.component.html',
  styleUrls: ['./edit-source.component.scss']
})
export class EditSourceComponent implements OnInit {

  private model: EditSourceModel = new EditSourceModel();
  private id: string;

  public source: ISourceEdit;
  public editForm: FormGroup;
  public badRequest: IBadRequest;
  public isLoading: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private titleService: Title,
    private router: Router,
    private sourceService: SourceService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.route.data
      .subscribe((data: { source: ISourceEdit }) => {
        this.source = data.source;
        this.titleService.setTitle(environment.projectName + ": Edit " + this.source.name);
        this.model.populateFromInterface(this.source);
        this.createForm();
      });
  }

  get name() { return this.editForm.get('name'); }

  public update(): void {
    if (this.editForm.invalid) {
      return;
    }
    this.badRequest = null;
    this.StartLoading();
    this.model.populateFromFormGroup(this.editForm);
    this.sourceService.update(this.id, this.model).subscribe(
      result => {
        if (this.badRequest) {
          this.badRequest = null;
        }
        this.router.navigate(['/admin/sources']);
      },
      error => {
        this.badRequest = error.error;
        this.StopLoading();
      },
      () => {
        this.StopLoading();
      }
    );
  }

  public remove(sourceObj): void {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Confirm Remove Source "' + sourceObj.name + '"',
        message: "Are you sure you want to remove this source?\nDeleting your source is permanent and will remove all content including sales."
      }
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
        this.badRequest = null;
        this.StartLoading();
        this.sourceService.delete(this.id).subscribe(
          result => {
            if (this.badRequest) {
              this.badRequest = null;
            }
            this.router.navigate(['/admin/sources']);
          },
          error => {
            this.badRequest = error.error;
            this.StopLoading();
          },
          () => {
            this.StopLoading();
          }
        );
      }
    });
  }




  private StartLoading() {
    this.isLoading = true;
    this.editForm.disable();
  }

  private StopLoading() {
    this.isLoading = false;
    this.editForm.enable();
  }

  private createForm(): void {
    this.editForm = new FormGroup({
      'name': new FormControl(this.model.name, Validators.required)
    });
  }

}
