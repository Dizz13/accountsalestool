import { Component, OnInit, ViewChild } from '@angular/core';
import { ISourceList } from 'src/app/interfaces/sources/source-list';
import { SourceService } from 'src/app/services/source.service';
import { QueryPageModel } from 'src/app/models/query-page-model';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';

@Component({
  selector: 'ast-source',
  templateUrl: './sources.component.html',
  styleUrls: [
    '../../scss/table.component.scss',
    './sources.component.scss'
  ]
})
export class SourcesComponent implements OnInit {

  public displayedColumns: string[] = ['name', 'created', 'action'];
  public dataSource: ISourceList[];
  public count: number;
  public model: QueryPageModel = new QueryPageModel();
  public total: number;
  public pageSizeOptions: number[] = [10, 25, 50, 100];
  public isLoading: boolean = false;

  @ViewChild('paginator') paginator: MatPaginator;

  constructor(
    private sourceService: SourceService
  ) { }

  ngOnInit(): void {
    this.model.pageNumber = 1;
    this.model.pageSize = this.pageSizeOptions[0];
    this.model.orderBy = 'name';
    this.model.isAscending = true;
    this.loadSources();
    this.loadCount();
  }

  public onPageChange(pageEvent: PageEvent) {
    this.model.pageNumber = pageEvent.pageIndex + 1;
    this.model.pageSize = pageEvent.pageSize;
    this.loadSources();
  }

  public onSortChange(sort: Sort) {
    this.model.pageNumber = 1;
    this.model.orderBy = sort.active;
    this.model.isAscending = sort.direction == 'asc';
    this.loadSources();
  }

  private loadSources(): void {
    this.sourceService.getPage(this.model).subscribe(
      result => {
        this.dataSource = result.data;
        this.total = result.total;
      },
      error => {
        
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  private loadCount(): void {
    this.isLoading = true;
    this.sourceService.count().subscribe(
      result => {
        this.count = result;
      },
      error => {
        
      }
    );
  }

}
