import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment';

import { AddSourceModel } from 'src/app/models/sources/add-source-model';
import { IBadRequest } from 'src/app/interfaces/bad-request';
import { ISelectResult } from 'src/app/interfaces/select-result';
import { SourceService } from 'src/app/services/source.service';

@Component({
  selector: 'ast-add-source',
  templateUrl: './add-source.component.html',
  styleUrls: ['./add-source.component.scss']
})
export class AddSourceComponent implements OnInit {

  private model: AddSourceModel = new AddSourceModel();

  public addForm: FormGroup;
  public sources: ISelectResult[];
  public badRequest: IBadRequest;
  public isLoading: boolean = false;

  constructor(
    private titleService: Title,
    private router: Router,
    private sourceService: SourceService
  ) { }

  ngOnInit() {
    this.titleService.setTitle(environment.projectName + ": Create source");
    this.loadSources();
    this.addForm = new FormGroup({
      'name': new FormControl(this.model.name, Validators.required),
    });
  }

  get name() { return this.addForm.get('name'); }
  
  add(): void {
    if (this.addForm.invalid) {
      return;
    }
    this.badRequest = null;
    this.startLoading();
    this.model.populateFromFormGroup(this.addForm);
    this.sourceService.add(this.model).subscribe(
      result => {
        if (this.badRequest) {
          this.badRequest = null;
        }
        this.router.navigate(['/admin/sources']);
      },
      error => {
        this.badRequest = error.error;
        this.stopLoading();
      },
      () => {
        this.stopLoading();
      }
    );
  }

  private startLoading() {
    this.isLoading = true;
    this.addForm.disable();
  }

  private stopLoading() {
    this.isLoading = false;
    this.addForm.enable();
  }

  private loadSources(): void {
    this.sourceService.getAll().subscribe(
      result => {
        this.sources = result;
      },
      error => {
        alert(error.message);
      }
    );
  }
}
