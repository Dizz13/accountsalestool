import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users/users.component';
import { AdminGuard } from 'src/app/auth/guards/admin.guard';
import { AddUserComponent } from './add-user/add-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { EditUserResolverService } from 'src/app/resolvers/users/edit-user-resolver.service';
import { ChangePasswordResolverService } from 'src/app/resolvers/users/change-password-resolver.service';

const routes: Routes = [
  {
    path: '',
    canActivate: [AdminGuard],
    children: [
      {
        path: '',
        canActivateChild: [AdminGuard],
        children: [
          { path: '', component: UsersComponent },
          { path: 'add', component: AddUserComponent },
          {
            path: ':id',
            component: EditUserComponent,
            resolve: {
              user: EditUserResolverService
            }
          },
          {
            path: ':id/change-password',
            component: ChangePasswordComponent,
            resolve: {
              user: ChangePasswordResolverService
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
