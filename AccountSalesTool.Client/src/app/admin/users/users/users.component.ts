import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';

import { UserService } from 'src/app/services/user.service';
import { IUserList } from 'src/app/interfaces/users/user-list';
import { QueryPageModel } from 'src/app/models/query-page-model';

@Component({
  selector: 'ast-users',
  templateUrl: './users.component.html',
  styleUrls: [
    '../../scss/table.component.scss',
    './users.component.scss'
  ]
})
export class UsersComponent implements OnInit {

  public model: QueryPageModel = new QueryPageModel();
  public displayedColumns: string[] = ['name', 'type', 'office', 'created', 'action'];
  public dataSource: IUserList[];
  public count: number;
  public isLoading: boolean = false;
  public total: number;
  public pageSizeOptions: number[] = [25, 50, 100];

  @ViewChild('paginator') paginator: MatPaginator;

  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.model.pageNumber = 1;
    this.model.pageSize = this.pageSizeOptions[0];
    this.model.orderBy = 'name';
    this.model.isAscending = true;
    this.loadUsers();
    this.loadCount();
  }

  public onPageChange(pageEvent: PageEvent) {
    this.model.pageNumber = pageEvent.pageIndex + 1;
    this.model.pageSize = pageEvent.pageSize;
    this.loadUsers();
  }

  public onSortChange(sort: Sort) {
    this.model.pageNumber = 1;
    this.model.orderBy = sort.active;
    this.model.isAscending = sort.direction == 'asc';
    this.loadUsers();
  }

  private loadUsers(): void {
    this.isLoading = true;
    this.userService.getPage(this.model).subscribe(
      result => {
        this.dataSource = result.data;
        this.total = result.total;
      },
      error => {
       
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  private loadCount(): void {
    this.userService.count().subscribe(
      result => {
        this.count = result;
      },
      error => {
       
      }
    );
  }

}
