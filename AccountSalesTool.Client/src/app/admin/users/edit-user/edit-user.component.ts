import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';

import { environment } from 'src/environments/environment';

import { IBadRequest } from 'src/app/interfaces/bad-request';
import { IUserEdit } from 'src/app/interfaces/users/user-edit';
import { EditUserModel } from 'src/app/models/users/edit-user-model';
import { UserType } from 'src/app/enums/user-type.enum';
import { UserService } from 'src/app/services/user.service';
import { ISelectResult } from 'src/app/interfaces/select-result';
import { OfficeService } from 'src/app/services/office.service';

@Component({
  selector: 'ast-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

  private model: EditUserModel = new EditUserModel();
  private id: string;

  public user: IUserEdit;
  public editForm: FormGroup;
  public userType = UserType;
  public offices: ISelectResult[];
  public badRequest: IBadRequest;
  public isLoading: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private titleService: Title,
    private router: Router,
    private userService: UserService,
    private officeService: OfficeService
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.route.data
      .subscribe((data: { user: IUserEdit }) => {
        this.user = data.user;
        this.titleService.setTitle(environment.projectName + ": Edit " + this.user.name);
        this.model.populateFromInterface(this.user);
        this.loadOffices();
        this.createForm();
      });
  }

  get name() { return this.editForm.get('name'); }
  get email() { return this.editForm.get('email'); }
  get type() { return this.editForm.get('type'); }
  get office() { return this.editForm.get('office'); }

  public update(): void {
    if (this.editForm.invalid) {
      return;
    }
    this.badRequest = null;
    this.startLoading();
    this.model.populateFromFormGroup(this.editForm);
    this.userService.update(this.id, this.model).subscribe(
      result => {
        if (this.badRequest) {
          this.badRequest = null;
        }
        this.router.navigate(['/admin/users']);
      },
      error => {
        this.badRequest = error.error;
        this.stopLoading();
      },
      () => {
        this.stopLoading();
      }
    );
  }

  public remove(): void {
    this.badRequest = null;
    this.startLoading();
    this.userService.delete(this.id).subscribe(
      result => {
        if (this.badRequest) {
          this.badRequest = null;
        }
        this.router.navigate(['/admin/users']);
      },
      error => {
        this.badRequest = error.error;
        this.stopLoading();
      },
      () => {
        this.stopLoading();
      }
    );
  }

  private startLoading() {
    this.isLoading = true;
    this.editForm.disable();
  }

  private stopLoading() {
    this.isLoading = false;
    this.editForm.enable();
  }

  private createForm(): void {
    this.editForm = new FormGroup({
      'name': new FormControl(this.model.name, Validators.required),
      'email': new FormControl(this.model.email, [
        Validators.required,
        Validators.email
      ]),
      'type': new FormControl(this.model.type, Validators.required),
      'office': new FormControl(this.model.officeId)
    });
  }

  private loadOffices(): void {
    this.officeService.getForUserWithCurrentAssignee(this.id).subscribe(
      result => {
        this.offices = result;
      },
      error => {
       
      }
    );
  }

}
