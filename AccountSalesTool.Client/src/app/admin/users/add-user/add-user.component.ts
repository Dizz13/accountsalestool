import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment';

import { AddUserModel } from 'src/app/models/users/add-user-model';
import { UserService } from 'src/app/services/user.service';
import { UserType } from 'src/app/enums/user-type.enum';
import { IBadRequest } from 'src/app/interfaces/bad-request';
import { ISelectResult } from 'src/app/interfaces/select-result';
import { OfficeService } from 'src/app/services/office.service';

@Component({
  selector: 'ast-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  private model: AddUserModel = new AddUserModel();

  public addForm: FormGroup;
  public userType = UserType;
  public offices: ISelectResult[];
  public badRequest: IBadRequest;
  public isPasswordHidden: boolean = true;
  public isLoading: boolean = false;

  constructor(
    private titleService: Title,
    private router: Router,
    private userService: UserService,
    private officeService: OfficeService
  ) { }

  ngOnInit() {
    this.titleService.setTitle(environment.projectName + ": Create user");
    this.setPredefinedValues();
    this.loadOffices();
    this.addForm = new FormGroup({
      'name': new FormControl(this.model.name, Validators.required),
      'email': new FormControl(this.model.email, [
        Validators.required,
        Validators.email
      ]),
      'password': new FormControl(this.model.password, Validators.required),
      'confirmation': new FormControl(this.model.password, [
        Validators.required,
        Validators.compose([this.validateAreEqual.bind(this)])
      ]),
      'type': new FormControl(this.model.type, Validators.required),
      'office': new FormControl(this.model.officeId)
    });
  }

  get name() { return this.addForm.get('name'); }
  get email() { return this.addForm.get('email'); }
  get password() { return this.addForm.get('password'); }
  get confirmation() { return this.addForm.get('confirmation'); }
  get type() { return this.addForm.get('type'); }
  get office() { return this.addForm.get('office'); }

  add(): void {
    if (this.addForm.invalid) {
      return;
    }
    this.badRequest = null;
    this.startLoading();
    this.model.populateFromFormGroup(this.addForm);
    this.userService.add(this.model).subscribe(
      result => {
        if (this.badRequest) {
          this.badRequest = null;
        }
        this.router.navigate(['/admin/users']);
      },
      error => {
        this.badRequest = error.error;
        this.stopLoading();
      },
      () => {
        this.stopLoading();
      }
    );
  }

  private startLoading() {
    this.isLoading = true;
    this.addForm.disable();
  }

  private stopLoading() {
    this.isLoading = false;
    this.addForm.enable();
  }

  private validateAreEqual(fieldControl: FormControl) {
    if (!this.addForm) {
      return null;
    }
    return fieldControl.value === this.password.value ? null : {
      notEqual: true
    };
  }

  private loadOffices(): void {
    this.officeService.getForUserWithoutCurrentAssignee().subscribe(
      result => {
        this.offices = result;
      },
      error => {
        
      }
    );
  }

  private setPredefinedValues(): void {
    this.model.type = UserType.Office;
  }

}
