import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { environment } from 'src/environments/environment';

import { ChangePasswordModel } from 'src/app/models/users/change-password-model';
import { IBadRequest } from 'src/app/interfaces/bad-request';
import { UserService } from 'src/app/services/user.service';
import { IChangePassword } from 'src/app/interfaces/users/change-password';

@Component({
  selector: 'ast-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  private model: ChangePasswordModel = new ChangePasswordModel();
  private id: string;

  public user: IChangePassword;
  public changePasswordForm: FormGroup;
  public badRequest: IBadRequest;
  public isPasswordHidden: boolean = true;
  public isLoading: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private titleService: Title,
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.route.data
      .subscribe((data: { user: IChangePassword }) => {
        this.user = data.user;
        this.titleService.setTitle(environment.projectName + ": Change password for " + this.user.name);
        this.createForm();
      });
  }

  get password() { return this.changePasswordForm.get('password'); }
  get confirmation() { return this.changePasswordForm.get('confirmation'); }

  public changePassword(): void {
    if (this.changePasswordForm.invalid) {
      return;
    }
    this.badRequest = null;
    this.StartLoading();
    this.model.populateFromFormGroup(this.changePasswordForm);
    this.userService.changePassword(this.id, this.model).subscribe(
      result => {
        if (this.badRequest) {
          this.badRequest = null;
        }
        this.router.navigate(['/admin/users']);
      },
      error => {
        this.badRequest = error.error;
        this.StopLoading();
      },
      () => {
        this.StopLoading();
      }
    );
  }

  private StartLoading() {
    this.isLoading = true;
    this.changePasswordForm.disable();
  }

  private StopLoading() {
    this.isLoading = false;
    this.changePasswordForm.enable();
  }

  private createForm(): void {
    this.changePasswordForm = new FormGroup({
      'password': new FormControl(this.model.password, Validators.required),
      'confirmation': new FormControl(this.model.password, [
        Validators.required,
        Validators.compose([this.validateAreEqual.bind(this)])
      ])
    });
  }

  private validateAreEqual(fieldControl: FormControl) {
    if (!this.changePasswordForm) {
      return null;
    }
    return fieldControl.value === this.password.value ? null : {
      notEqual: true
    };
  }

}
