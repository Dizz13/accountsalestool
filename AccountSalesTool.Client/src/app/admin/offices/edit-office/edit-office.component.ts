import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { environment } from 'src/environments/environment';

import { EditOfficeModel } from 'src/app/models/offices/edit-office-model';
import { IOfficeEdit } from 'src/app/interfaces/offices/office-edit';
import { ISelectResult } from 'src/app/interfaces/select-result';
import { IBadRequest } from 'src/app/interfaces/bad-request';
import { UserService } from 'src/app/services/user.service';
import { OfficeService } from 'src/app/services/office.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'ast-edit-office',
  templateUrl: './edit-office.component.html',
  styleUrls: ['./edit-office.component.scss']
})
export class EditOfficeComponent implements OnInit {

  private model: EditOfficeModel = new EditOfficeModel();

  public id: string;
  public office: IOfficeEdit;
  public editForm: FormGroup;
  public users: ISelectResult[];
  public badRequest: IBadRequest;
  public isLoading: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private titleService: Title,
    private router: Router,
    private userService: UserService,
    private officeService: OfficeService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.route.data
      .subscribe((data: { office: IOfficeEdit }) => {
        this.office = data.office;
        this.titleService.setTitle(environment.projectName + ": Edit " + this.office.name);
        this.model.populateFromInterface(this.office);
        this.loadUsers();
        this.createForm();
      });
  }

  get name() { return this.editForm.get('name'); }
  get user() { return this.editForm.get('user'); }

  public update(): void {
    if (this.editForm.invalid) {
      return;
    }
    this.badRequest = null;
    this.startLoading();
    this.model.populateFromFormGroup(this.editForm);
    this.officeService.update(this.id, this.model).subscribe(
      result => {
        if (this.badRequest) {
          this.badRequest = null;
        }
        this.router.navigate(['/admin/offices']);
      },
      error => {
        this.badRequest = error.error;
        this.stopLoading();
      },
      () => {
        this.stopLoading();
      }
    );
  }


  public remove(officeObj): void {

    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Confirm Remove Office "' + officeObj.name + '"',
        message: "Are you sure you want to remove this office?\nDeleting your office is permanent and will remove all content including sales."
      }
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result === true) {
        this.badRequest = null;
        this.startLoading();
        this.officeService.delete(this.id).subscribe(
          result => {
            if (this.badRequest) {
              this.badRequest = null;
            }
            this.router.navigate(['/admin/offices']);
          },
          error => {
            this.badRequest = error.error;
            this.stopLoading();
          },
          () => {
            this.stopLoading();
          }
        );
      }
    });
  }




  private startLoading() {
    this.isLoading = true;
    this.editForm.disable();
  }

  private stopLoading() {
    this.isLoading = false;
    this.editForm.enable();
  }

  private createForm(): void {
    this.editForm = new FormGroup({
      'name': new FormControl(this.model.name, [
        Validators.required,
        Validators.maxLength(7)
      ]),
      'user': new FormControl(this.model.userId)
    });
  }

  private loadUsers(): void {
    this.userService.getForOfficeWithCurrentAssignee(this.id).subscribe(
      result => {
        this.users = result;
      },
      error => {
        
      }
    );
  }

}
