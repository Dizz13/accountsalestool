import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminGuard } from 'src/app/auth/guards/admin.guard';
import { OfficesComponent } from './offices/offices.component';
import { AddOfficeComponent } from './add-office/add-office.component';
import { OfficeDetailsComponent } from './office-details/office-details.component';
import { EditOfficeComponent } from './edit-office/edit-office.component';
import { OfficeSourcesComponent } from './office-sources/office-sources.component';
import { OfficeDetailsResolverService } from 'src/app/resolvers/offices/office-details-resolver.service';
import { OfficeEditResolverService } from 'src/app/resolvers/offices/office-edit-resolver.service';
import { OfficeSourcesResolverService } from 'src/app/resolvers/offices/office-sources-resolver.service';
import { SourcesListResolverService } from 'src/app/resolvers/sources/sources-list-resolver.service';

const routes: Routes = [
  {
    path: '',
    canActivate: [AdminGuard],
    children: [
      {
        path: '',
        canActivateChild: [AdminGuard],
        children: [
          { path: '', component: OfficesComponent },
          { path: 'add', component: AddOfficeComponent },
          {
            path: ':id',
            component: OfficeDetailsComponent,
            resolve: {
              office: OfficeDetailsResolverService
            }
          },
          {
            path: ':id/edit',
            component: EditOfficeComponent,
            resolve: {
              office: OfficeEditResolverService
            }
          },
          {
            path: ':id/sources',
            component: OfficeSourcesComponent,
            resolve: {
              office: OfficeSourcesResolverService,
              sources: SourcesListResolverService
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OfficesRoutingModule { }
