import { Component, OnInit, ViewChild } from '@angular/core';
import { OfficeService } from 'src/app/services/office.service';
import { IOfficeList } from 'src/app/interfaces/offices/office-list';
import { QueryPageModel } from 'src/app/models/query-page-model';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
@Component({
  selector: 'ast-offices',
  templateUrl: './offices.component.html',
  styleUrls: [
    '../../scss/table.component.scss',
    './offices.component.scss'
  ]
})
export class OfficesComponent implements OnInit {

  public displayedColumns: string[] = ['name', 'user', 'created', 'updated', 'action'];
  public dataSource: IOfficeList[];
  public count: number;
  public model: QueryPageModel = new QueryPageModel();
  public total: number;
  public pageSizeOptions: number[] = [10, 25, 50, 100];
  public isLoading: boolean = false;

  @ViewChild('paginator') paginator: MatPaginator;

  constructor(
    private officeService: OfficeService
  ) { }

  ngOnInit(): void {
    this.model.pageNumber = 1;
    this.model.pageSize = this.pageSizeOptions[0];
    this.model.orderBy = 'name';
    this.model.isAscending = true;
    this.loadOffices();
    this.loadCount();
  }

  private loadOffices(): void {
    this.isLoading = true;
    this.officeService.getPage(this.model).subscribe(
      result => {
        this.dataSource = result.data;
        this.total = result.total;
      },
      error => {
        this.isLoading = false;
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  private loadCount(): void {
    this.officeService.count().subscribe(
      result => {
        this.count = result;
      },
      error => {
      }
    );
  }

  public onPageChange(pageEvent: PageEvent) {
    this.model.pageNumber = pageEvent.pageIndex + 1;
    this.model.pageSize = pageEvent.pageSize;
    this.loadOffices();
  }

  public onSortChange(sort: Sort) {
    this.model.pageNumber = 1;
    this.model.orderBy = sort.active;
    this.model.isAscending = sort.direction == 'asc';
    this.loadOffices();
  }

}
