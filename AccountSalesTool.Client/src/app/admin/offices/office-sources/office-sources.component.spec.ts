import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfficeSourcesComponent } from './office-sources.component';

describe('OfficeSourcesComponent', () => {
  let component: OfficeSourcesComponent;
  let fixture: ComponentFixture<OfficeSourcesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfficeSourcesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfficeSourcesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
