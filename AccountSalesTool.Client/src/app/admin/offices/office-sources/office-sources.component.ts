import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';

import { environment } from 'src/environments/environment';

import { IBadRequest } from 'src/app/interfaces/bad-request';
import { OfficeService } from 'src/app/services/office.service';
import { ISelectResult } from 'src/app/interfaces/select-result';
import { IOfficeSources } from 'src/app/interfaces/offices/office-sources';
import { OfficeSourcesModel } from 'src/app/models/offices/office-sources-model';

@Component({
  selector: 'ast-office-sources',
  templateUrl: './office-sources.component.html',
  styleUrls: ['./office-sources.component.scss']
})
export class OfficeSourcesComponent implements OnInit {

  private model: OfficeSourcesModel = new OfficeSourcesModel();

  public id: string;
  public office: IOfficeSources;
  public sources: ISelectResult[];
  public selected: string[] = [];
  public badRequest: IBadRequest;
  public isLoading: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private titleService: Title,
    private router: Router,
    private officeService: OfficeService
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.route.data
      .subscribe((data: { office: IOfficeSources, sources: ISelectResult[] }) => {
        this.office = data.office;
        this.sources = data.sources;
        this.titleService.setTitle(environment.projectName + ": Sources of " + this.office.name);
      });
  }

  public isSelected(source: ISelectResult): boolean {
    return this.office.sources.includes(source.id);
  }

  public select(source: ISelectResult): void {
    let index = this.office.sources.indexOf(source.id);
    if (index > -1) {
      this.office.sources.splice(index, 1);
    }
    else {
      this.office.sources.push(source.id);
    }
  }

  public update(): void {
    this.badRequest = null;
    this.model.sources = this.office.sources;
    this.startLoading();
    this.officeService.updateSources(this.id, this.model).subscribe(
      result => {
        if (this.badRequest) {
          this.badRequest = null;
        }
        this.router.navigate(['/admin/offices']);
      },
      error => {
        this.badRequest = error.error;
        this.stopLoading();
      },
      () => {
        this.stopLoading();
      }
    );
  }

  private startLoading() {
    this.isLoading = true;
  }

  private stopLoading() {
    this.isLoading = false;
  }

}
