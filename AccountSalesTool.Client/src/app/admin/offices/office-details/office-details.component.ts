import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { environment } from 'src/environments/environment';

import { IOfficeDetails } from 'src/app/interfaces/offices/office-details';

@Component({
  selector: 'ast-office-details',
  templateUrl: './office-details.component.html',
  styleUrls: ['./office-details.component.scss']
})
export class OfficeDetailsComponent implements OnInit {

  private id: string;

  public office: IOfficeDetails;

  constructor(
    private route: ActivatedRoute,
    private titleService: Title
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.route.data
      .subscribe((data: { office: IOfficeDetails }) => {
        this.office = data.office;
        this.titleService.setTitle(environment.projectName + ": Office " + this.office.name);
      });
  }

}
