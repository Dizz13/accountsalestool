import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { OfficesRoutingModule } from './offices-routing.module';
import { OfficesComponent } from './offices/offices.component';
import { AddOfficeComponent } from './add-office/add-office.component';
import { EditOfficeComponent } from './edit-office/edit-office.component';
import { OfficeDetailsComponent } from './office-details/office-details.component';
import { OfficeSourcesComponent } from './office-sources/office-sources.component';
import { MaterialModule } from 'src/app/material-module';


@NgModule({
  declarations: [
    OfficesComponent, 
    AddOfficeComponent, 
    EditOfficeComponent, 
    OfficeDetailsComponent, 
    OfficeSourcesComponent
  ],
  imports: [
    CommonModule,
    OfficesRoutingModule,
    ReactiveFormsModule,
    MaterialModule
  ]
})
export class OfficesModule { }
