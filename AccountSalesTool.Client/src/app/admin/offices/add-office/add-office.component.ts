import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { environment } from 'src/environments/environment';

import { AddOfficeModel } from 'src/app/models/offices/add-office-model';
import { IBadRequest } from 'src/app/interfaces/bad-request';
import { ISelectResult } from 'src/app/interfaces/select-result';
import { UserService } from 'src/app/services/user.service';
import { OfficeService } from 'src/app/services/office.service';

@Component({
  selector: 'ast-add-office',
  templateUrl: './add-office.component.html',
  styleUrls: ['./add-office.component.scss']
})
export class AddOfficeComponent implements OnInit {

  private model: AddOfficeModel = new AddOfficeModel();

  public addForm: FormGroup;
  public users: ISelectResult[];
  public badRequest: IBadRequest;
  public isLoading: boolean = false;

  constructor(
    private titleService: Title,
    private router: Router,
    private userService: UserService,
    private officeService: OfficeService
  ) { }

  ngOnInit() {
    this.titleService.setTitle(environment.projectName + ": Create office");
    this.setPredefinedValues();
    this.loadUsers();
    this.addForm = new FormGroup({
      'name': new FormControl(this.model.name, 
        [
          Validators.required,
          Validators.maxLength(7)
        ]),
      'user': new FormControl(this.model.userId)
    });
  }

  get name() { return this.addForm.get('name'); }
  get user() { return this.addForm.get('user'); }

  add(): void {
    if (this.addForm.invalid) {
      return;
    }
    this.badRequest = null;
    this.startLoading();
    this.model.populateFromFormGroup(this.addForm);
    this.officeService.add(this.model).subscribe(
      result => {
        if (this.badRequest) {
          this.badRequest = null;
        }
        this.router.navigate(['/admin/offices/' + result.id]);
      },
      error => {
        this.badRequest = error.error;
        this.stopLoading();
      },
      () => {
        this.stopLoading();
      }
    );
  }

  private startLoading() {
    this.isLoading = true;
    this.addForm.disable();
  }

  private stopLoading() {
    this.isLoading = false;
    this.addForm.enable();
  }

  private loadUsers(): void {
    this.userService.getForOfficeWithoutCurrentAssignee().subscribe(
      result => {
        this.users = result;
      },
      error => {
        alert(error.message);
      }
    );
  }

  private setPredefinedValues(): void {
    this.model.userId = null;
  }

}
