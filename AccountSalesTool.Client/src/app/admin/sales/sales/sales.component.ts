import { Component, OnInit } from '@angular/core';
import { SalesService } from 'src/app/services/sales.service';
import { ISalesList } from 'src/app/interfaces/sales/sales-list';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Sort } from '@angular/material/sort';
import { IOfficeDetails } from 'src/app/interfaces/offices/office-details';
import { IOfficeList } from 'src/app/interfaces/offices/office-list';
import { ISourceList } from 'src/app/interfaces/sources/source-list';
import { IState } from 'src/app/interfaces/states/state';
import { SourceService } from 'src/app/services/source.service';
import { OfficeService } from 'src/app/services/office.service';
import { StateService } from 'src/app/services/state.service';
import { FilterSaleModel } from 'src/app/models/sales/filter-sale-model';
import { PageEvent } from '@angular/material/paginator';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'ast-sales',
  templateUrl: './sales.component.html',
  styleUrls: [
    '../../scss/table.component.scss',
    './sales.component.scss'
  ]
})
export class SalesComponent implements OnInit {

  public displayedColumns: string[] = ['phone', 'office', 'source', 'email', 'state', 'date', 'type'];
  public dataSource: ISalesList[];
  public count: number;

  public offices:IOfficeList[];
  public sources:ISourceList[];
  public states:IState[];
  public isLoading: boolean = false;
  public valueAll: string = '';

  public model: FilterSaleModel = new FilterSaleModel();
  public total: number;
  public pageSizeOptions: number[] = [25, 50, 100];

  public filtersForm:FormGroup;

  constructor(
    private salesService: SalesService,
    private sourceService: SourceService,
    private officeService: OfficeService,
    private stateService: StateService,
    private datepipe: DatePipe
  ) { }

  ngOnInit(): void {
    this.initFilterModel();
    this.initForm();
    
    this.loadSales();
    this.loadCount();

    this.loadOffices();
    this.loadSources();
    this.loadStates();

    
  }

  initForm() {
    this.filtersForm = new FormGroup({
      'startDate': new FormControl(this.model.startDate, Validators.required),
      'endDate': new FormControl(this.model.endDate, Validators.required),
      'office': new FormControl(this.model.officeId),
      'source': new FormControl(this.model.sourceId),
      'state': new FormControl(this.model.stateId),
    });

    this.filtersForm.controls['office'].setValue(this.valueAll);
    this.filtersForm.controls['source'].setValue(this.valueAll);
    this.filtersForm.controls['state'].setValue(this.valueAll);
  }

  private initFilterModel(): void {
    this.model.startDate = new Date(new Date().toDateString());
    this.model.endDate = new Date(new Date().toDateString());
    
    this.model.pageNumber = 1;
    this.model.pageSize = this.pageSizeOptions[0];
    this.model.orderBy = 'created';
    this.model.isAscending = true;
  }

  private loadSales(): void {
    if (this.filtersForm.invalid) {
      return;
    }
    
    this.startLoading();
    
    this.model.populateFromFormGroup(this.filtersForm);

    this.salesService.getPage(this.model).subscribe(
      result => {
        this.dataSource = result.data;
        this.total = result.total;

        this.stopLoading;
      },
      error => {
        this.stopLoading;
      },
      () => {
        this.stopLoading();
      }
    );
  }

  private loadCount(): void {
    this.startLoading();
    this.salesService.count().subscribe(
      result => {
        this.count = result;
        this.stopLoading;
      },
      error => {
        this.stopLoading;
      }
    );
  }

  private loadOffices(): void {
    this.startLoading();
    this.officeService.getAll().subscribe(
      result => {
        this.offices = result;
        this.stopLoading;
      },
      error => {
        this.stopLoading;
      }
    );
  }

  private loadSources(): void {
    this.startLoading();
    this.sourceService.getAll().subscribe(
      result => {
        this.sources = result;
        this.stopLoading;
      },
      error => {
        this.stopLoading;
      }
    );
  }

  private loadStates(): void {
    this.startLoading();
    this.stateService.getAll().subscribe(
      result => {
        this.states = result;
        this.stopLoading;
      },
      error => {
        this.stopLoading;
      }
    );
  }

  private startLoading() {
    this.isLoading = true;
    this.filtersForm.disable();
  }

  private stopLoading() {
    this.isLoading = false;
    this.filtersForm.enable();
  }

  public onPageChange(pageEvent: PageEvent) {
    this.model.pageNumber = pageEvent.pageIndex + 1;
    this.model.pageSize = pageEvent.pageSize;
    this.loadSales();
  }

  public onSortChange(sort: Sort) {
    this.model.pageNumber = 1;
    this.model.orderBy = sort.active;
    this.model.isAscending = sort.direction == 'asc';
    this.loadSales();
  }

  onFilter() {
    this.loadSales();
  } 

  exportFile() {
    
    this.startLoading();
    this.model.populateFromFormGroup(this.filtersForm);

    this.salesService.exportFile( this.model).subscribe(
      result => {
        var fileName = 'sales_ ' + this.datepipe.transform(new Date(),'yyyyMMdd-HHmm') + '.xlsx';
        this.saveFile(result.body, fileName);
        this.stopLoading();
      },
      error => {
        this.stopLoading();
      },
      () => {
        this.stopLoading();
      }
    );
  }

  saveFile(response: Blob, fileName: string): void {
    let blob = new Blob([response], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(blob);
      return;
    }

    const data = window.URL.createObjectURL(blob);

    var link = document.createElement('a');
    link.href = data;
    link.download = fileName;
    link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

    setTimeout(function () {
      window.URL.revokeObjectURL(data);
      link.remove();
    }, 500);
  }
}
