import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OfficeComponent } from './office/office.component';
import { OfficeGuard } from '../auth/guards/office.guard';

const routes: Routes = [
  {
    path: 'office',
    component: OfficeComponent,
    canActivate: [OfficeGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OfficeRoutingModule { }
