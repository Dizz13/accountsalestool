import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { ISource } from 'src/app/interfaces/sources/source';
import { AddSaleModel } from 'src/app/models/sales/add-sale-model';
import { SourceService } from 'src/app/services/source.service';
import { SalesService } from 'src/app/services/sales.service';
import { StateService } from 'src/app/services/state.service';
import { IBadRequest } from 'src/app/interfaces/bad-request';
import { IState } from 'src/app/interfaces/states/state';

@Component({
  selector: 'ast-office',
  templateUrl: './office.component.html',
  styleUrls: ['./office.component.scss']
})
export class OfficeComponent implements OnInit {

  public addForm: FormGroup;
  private model: AddSaleModel = new AddSaleModel();

  public userName:string = '';
  public officeName:string = '';

  public salesToday: number;
  public isLoading: boolean = false;

  public states: IState[] = [];
  public sources: ISource[] = [];

  public badRequest: IBadRequest;

  constructor(private authService:AuthService,  
    private sourceService: SourceService, 
    private salesService: SalesService,
    private stateService: StateService) { }

  ngOnInit(): void {
    this.userName = this.authService.getName();
    this.officeName = this.authService.getOffice()

    this.initForm();

    this.getSalesCount();
    this.getStates();
    this.getSources();
   
  }

  private initForm() {
    this.addForm = new FormGroup({
      'phone': new FormControl(this.model.phone,
        [
          Validators.pattern(/^\(\d{3}\)\s\d{3}-\d{4}$/),
          Validators.required
        ]),
      'email': new FormControl(this.model.email, [
        Validators.required,
        Validators.email
      ]),
      'state': new FormControl(this.model.stateId, Validators.required),
      'source': new FormControl(this.model.sourceId, Validators.required),
      'office': new FormControl(this.authService.getOfficeId(), Validators.required),
    });

    this.phone.markAsTouched();
    this.email.markAsTouched();
    this.state.markAsTouched();
    this.source.markAsTouched();
    this.office.markAsTouched();

  }

  private getSalesCount()
  {
    this.startLoading();
    this.badRequest = null;
    
    this.salesService.countToday(this.authService.getOfficeId()).subscribe(
      result => {
          this.salesToday = result;

          if (this.badRequest) {
            this.badRequest = null;
          }
      },
      error => {
        this.badRequest = error.error;
        this.stopLoading();
      },
      () => {
        this.stopLoading();
      }
    );
  }

  private getSources()
  {
    this.startLoading();
    this.badRequest = null;
    
    this.sourceService.getByOffice(this.authService.getOfficeId()).subscribe(
      result => {
        this.sources = [...result];

        if (this.badRequest) {
          this.badRequest = null;
        }
      },
      error => {
        this.badRequest = error.error;
        this.stopLoading();
      },
      () => {
        this.stopLoading();
      }
    );
  }

  private getStates()
  {
    this.startLoading();
    this.badRequest = null;
    
    this.stateService.getAll().subscribe(
      result => {
        this.states = [...result];

        if (this.badRequest) {
          this.badRequest = null;
        }
      },
      error => {
        this.badRequest = error.error;
        this.stopLoading();
      },
      () => {
        this.stopLoading();
      }
    );
  }

  get phone() { return this.addForm.get('phone'); }
  get email() { return this.addForm.get('email'); }
  get state() { return this.addForm.get('state'); }
  get source() { return this.addForm.get('source'); }
  get office() { return this.addForm.get('office'); }

  private cleanForm()
  {
    this.addForm.reset();

    this.email.markAsUntouched();
  }

  private startLoading() {
    this.isLoading = true;
    this.addForm.disable();
  }

  private stopLoading() {
    this.isLoading = false;
    this.addForm.enable();
  }

  add(): void {
    if (this.addForm.invalid) {
      return;
    }

    this.badRequest = null;
    this.startLoading();
    this.model.populateFromFormGroup(this.addForm);

    this.salesService.add(this.model).subscribe(
      result => {
        if (this.badRequest) {
          this.badRequest = null;
        }
        this.cleanForm();
        this.getSalesCount();
      },
      error => {
        this.badRequest = error.error;
        this.stopLoading();
      },
      () => {
        this.stopLoading();
      }
    );
  }

}
