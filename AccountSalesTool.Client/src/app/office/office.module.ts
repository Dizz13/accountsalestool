import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OfficeRoutingModule } from './office-routing.module';
import { OfficeComponent } from './office/office.component';
import { MaterialModule } from '../material-module';
import { ReactiveFormsModule } from '@angular/forms';
import { PhoneMaskDirective } from '../directives/phone-mask.directive';


@NgModule({
  declarations: [OfficeComponent, PhoneMaskDirective],
  imports: [
    CommonModule,
    OfficeRoutingModule,
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class OfficeModule { }
