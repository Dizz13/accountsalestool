import { TestBed } from '@angular/core/testing';

import { OfficeGuard } from './office.guard';

describe('OfficeGuard', () => {
  let guard: OfficeGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(OfficeGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
