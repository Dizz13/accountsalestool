export class Policies {
    public static readonly Admin: string = 'Admin';
    public static readonly Office: string = 'Office';
    public static readonly Report: string = 'Report';
}
