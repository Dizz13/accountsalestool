import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TickerRoutingModule } from './ticker-routing.module';
import { TickerComponent } from './ticker/ticker.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    TickerComponent
  ],
  imports: [
    CommonModule,
    TickerRoutingModule,
    BrowserAnimationsModule
  ]
})
export class TickerModule { }
