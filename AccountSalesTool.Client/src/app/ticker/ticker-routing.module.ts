import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TickerComponent } from './ticker/ticker.component';
import { ReportGuard } from '../auth/guards/report.guard';

const routes: Routes = [
  {
    path: 'ticker',
    component: TickerComponent,
    canActivate: [ReportGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TickerRoutingModule { }
