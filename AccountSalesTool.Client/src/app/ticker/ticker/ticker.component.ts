import { Component, OnInit, OnDestroy } from '@angular/core';
import { trigger, transition, style, animate } from '@angular/animations';

import { TickerService } from 'src/app/services/ticker.service';
import { IOfficeTicker } from 'src/app/interfaces/offices/office-ticker';

@Component({
  selector: 'ast-ticker',
  templateUrl: './ticker.component.html',
  styleUrls: ['./ticker.component.scss'],
  animations: [
    trigger('items', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('1s linear',
          style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ 'margin-left': '0px', opacity: 1 }),
        animate('1s linear',
          style({ 'margin-left': '{{margin}}', opacity: 0 }))
      ], { params: { margin: `${-TickerComponent.itemWidth}${TickerComponent.widthUnits}` } })
    ])
  ]
})
export class TickerComponent implements OnInit, OnDestroy {

  public static readonly size: number = 5;
  public static readonly itemWidth: number = 16;
  public static readonly widthUnits: string = 'vw';

  private dataEventSubscription = null;
  private updateEventSubscription = null;
  private index: number;

  public data: IOfficeTicker[];

  get itemStyleWidth() { return `${TickerComponent.itemWidth}${TickerComponent.widthUnits}`; }
  get negativeItemStyleWidth() { return `${-TickerComponent.itemWidth}${TickerComponent.widthUnits}`; }
  get containerStyleWidth() { return `${TickerComponent.itemWidth * TickerComponent.size}${TickerComponent.widthUnits}`; }

  constructor(
    public tickerService: TickerService
  ) { }

  ngOnInit(): void {
    this.dataEventSubscription = this.tickerService.dataRecievedEvent.subscribe(() => {
      var length = this.tickerService.data.length;
      this.index = (length > TickerComponent.size ? TickerComponent.size : length) - 1;
      this.data = this.tickerService.data.slice(0, this.index);
      setInterval(() => this.move(), 1000);
      this.dataEventSubscription.unsubscribe();
    });
    this.updateEventSubscription = this.tickerService.updateRecievedEvent.subscribe(() => {

    });
    this.tickerService.startConnection();
    this.tickerService.addTickerListener();
  }

  ngOnDestroy(): void {
    if (this.dataEventSubscription) {
      this.dataEventSubscription.unsubscribe();
    }
    if (this.updateEventSubscription) {
      this.updateEventSubscription.unsubscribe();
    }
    this.tickerService.stopConnection();
  }

  private move(): void {
    var newIndex = ++this.index % this.tickerService.data.length;
    var newItem = this.tickerService.data[newIndex];
    this.data.shift();
    setTimeout(() => this.data.push(newItem), 0);
  }

}
