import { FormGroup } from '@angular/forms';

export class AddSourceModel {
    name: string;

    populateFromFormGroup(formGroup: FormGroup): void {
        this.name = formGroup.get('name').value;
    }
}
