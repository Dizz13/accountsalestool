import { FormGroup } from '@angular/forms';
import { ISourceEdit } from 'src/app/interfaces/sources/source-edit';

export class EditSourceModel implements ISourceEdit {
    name: string;

    populateFromFormGroup(formGroup: FormGroup): void {
        this.name = formGroup.get('name').value;
    }

    populateFromInterface(dto: ISourceEdit): void {
        this.name = dto.name;
    }
}
