import { FormGroup } from '@angular/forms';

import { UserType } from 'src/app/enums/user-type.enum';
 
export class AddUserModel {
    name: string;
    email: string;
    password: string;
    type: UserType;
    officeId: string;

    populateFromFormGroup(formGroup: FormGroup): void {
        this.name = formGroup.get('name').value;
        this.email = formGroup.get('email').value;
        this.password = formGroup.get('password').value;
        this.type = formGroup.get('type').value;
        this.officeId = this.type == UserType.Office ? formGroup.get('office').value : null;
    }
}
