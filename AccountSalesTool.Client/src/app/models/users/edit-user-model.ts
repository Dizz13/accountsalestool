import { FormGroup } from '@angular/forms';

import { IUserEdit } from 'src/app/interfaces/users/user-edit';
import { UserType } from 'src/app/enums/user-type.enum';

export class EditUserModel implements IUserEdit {
    name: string;
    email: string;
    type: UserType;
    officeId: string;

    populateFromFormGroup(formGroup: FormGroup): void {
        this.name = formGroup.get('name').value;
        this.email = formGroup.get('email').value;
        this.type = formGroup.get('type').value;
        this.officeId = this.type == UserType.Office ? formGroup.get('office').value : null;
    }

    populateFromInterface(dto: IUserEdit): void {
        this.name = dto.name;
        this.email = dto.email;
        this.type = dto.type;
        this.officeId = dto.officeId;
    }
}
