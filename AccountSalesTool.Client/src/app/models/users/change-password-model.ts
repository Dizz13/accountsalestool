import { FormGroup } from '@angular/forms';

export class ChangePasswordModel {
    password: string;

    populateFromFormGroup(formGroup: FormGroup): void {
        this.password = formGroup.get('password').value;
    }
}
