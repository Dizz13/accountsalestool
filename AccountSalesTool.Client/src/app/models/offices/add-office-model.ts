import { FormGroup } from '@angular/forms';

export class AddOfficeModel {
    name: string;
    userId: string;

    populateFromFormGroup(formGroup: FormGroup): void {
        this.name = formGroup.get('name').value;
        this.userId = formGroup.get('user').value;
    }
}
