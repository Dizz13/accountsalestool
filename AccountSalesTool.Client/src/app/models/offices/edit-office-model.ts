import { IOfficeEdit } from 'src/app/interfaces/offices/office-edit';
import { FormGroup } from '@angular/forms';

export class EditOfficeModel implements IOfficeEdit {
    name: string;
    userId: string;

    populateFromFormGroup(formGroup: FormGroup): void {
        this.name = formGroup.get('name').value;
        this.userId = formGroup.get('user').value;
    }

    populateFromInterface(dto: IOfficeEdit): void {
        this.name = dto.name;
        this.userId = dto.userId;
    }
}
