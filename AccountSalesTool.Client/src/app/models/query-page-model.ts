export class QueryPageModel {
    pageNumber: number;
    pageSize: number;
    orderBy: string;
    isAscending: boolean;
}
