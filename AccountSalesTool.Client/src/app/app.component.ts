import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

import { AuthService } from './auth/auth.service';

@Component({
  selector: 'ast-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public title: string;

  constructor(
    public authService: AuthService
  ) { }

  ngOnInit(): void {
    this.title = environment.projectName;
  }
}
