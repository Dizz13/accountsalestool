export interface IAccount {
    name: string;
    email: string;
    role: string;
    officeId: string;
    office: string;
    token: string;
}
