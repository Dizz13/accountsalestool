export interface IOfficeTicker {
    id: string;
    name: string;
    sales: number;
}
