export interface IOfficeDetails {
    name: string;
    user: string;
    created: string;
    updated: string;
}
