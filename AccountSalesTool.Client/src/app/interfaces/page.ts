export interface IPage<T> {
    data: T[];
    number: number;
    size: number;
    total: number;
}
