export interface ISalesList {
    id: string;
    phone: string;
    email: string;
    created: string;
    updated: string;
    office: string;
    source: string;
    state: string;
    type: string;
}
