export interface IUserList {
    id: string;
    name: string;
    type: string;
    office: string;
    created: string;
}
