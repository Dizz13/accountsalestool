import { UserType } from 'src/app/enums/user-type.enum';

export interface IUserEdit {
    name: string;
    email: string;
    type: UserType;
    officeId: string;
}
