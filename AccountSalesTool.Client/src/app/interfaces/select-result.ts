export interface ISelectResult {
    id: string;
    name: string;
}
