import { TestBed } from '@angular/core/testing';

import { EditUserResolverService } from './edit-user-resolver.service';

describe('EditUserResolverService', () => {
  let service: EditUserResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EditUserResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
