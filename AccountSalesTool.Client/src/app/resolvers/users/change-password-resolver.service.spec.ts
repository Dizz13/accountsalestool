import { TestBed } from '@angular/core/testing';

import { ChangePasswordResolverService } from './change-password-resolver.service';

describe('ChangePasswordResolverService', () => {
  let service: ChangePasswordResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChangePasswordResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
