import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { IChangePassword } from 'src/app/interfaces/users/change-password';
import { UserService } from 'src/app/services/user.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChangePasswordResolverService implements Resolve<IChangePassword> {

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IChangePassword | Observable<IChangePassword> | Promise<IChangePassword> {
    const id = route.paramMap.get('id');
    return this.userService.getChangePassword(id)
      .pipe(map(user => {
        if (user) {
          return user;
        } else {
          this.router.navigate(['/admin/users']);
          return null;
        }
      }));
  }
}
