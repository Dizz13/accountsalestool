import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { IUserEdit } from 'src/app/interfaces/users/user-edit';
import { UserService } from 'src/app/services/user.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EditUserResolverService implements Resolve<IUserEdit> {

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IUserEdit | Observable<IUserEdit> | Promise<IUserEdit> {
    const id = route.paramMap.get('id');
    return this.userService.get(id)
      .pipe(map(user => {
        if (user) {
          return user;
        } else {
          this.router.navigate(['/admin/users']);
          return null;
        }
      }));
  }
}
