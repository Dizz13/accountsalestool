import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { IOfficeSources } from 'src/app/interfaces/offices/office-sources';
import { OfficeService } from 'src/app/services/office.service';

@Injectable({
  providedIn: 'root'
})
export class OfficeSourcesResolverService implements Resolve<IOfficeSources> {

  constructor(
    private officeService: OfficeService,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IOfficeSources | Observable<IOfficeSources> | Promise<IOfficeSources> {
    const id = route.paramMap.get('id');
    return this.officeService.getSources(id)
      .pipe(map(office => {
        if (office) {
          return office;
        } else {
          this.router.navigate(['/admin/offices']);
          return null;
        }
      }));
  }
}
