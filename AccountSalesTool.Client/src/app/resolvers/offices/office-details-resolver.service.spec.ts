import { TestBed } from '@angular/core/testing';

import { OfficeDetailsResolverService } from './office-details-resolver.service';

describe('OfficeDetailsResolverService', () => {
  let service: OfficeDetailsResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OfficeDetailsResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
