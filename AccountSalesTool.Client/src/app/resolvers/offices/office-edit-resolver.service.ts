import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { IOfficeEdit } from 'src/app/interfaces/offices/office-edit';
import { OfficeService } from 'src/app/services/office.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OfficeEditResolverService implements Resolve<IOfficeEdit> {

  constructor(
    private officeService: OfficeService,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IOfficeEdit | Observable<IOfficeEdit> | Promise<IOfficeEdit> {
    const id = route.paramMap.get('id');
    return this.officeService.getEdit(id)
      .pipe(map(office => {
        if (office) {
          return office;
        } else {
          this.router.navigate(['/admin/offices']);
          return null;
        }
      }));
  }
}
