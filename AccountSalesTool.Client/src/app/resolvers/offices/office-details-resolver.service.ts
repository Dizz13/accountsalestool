import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { OfficeService } from 'src/app/services/office.service';
import { IOfficeDetails } from 'src/app/interfaces/offices/office-details';

@Injectable({
  providedIn: 'root'
})
export class OfficeDetailsResolverService implements Resolve<IOfficeDetails> {

  constructor(
    private officeService: OfficeService,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IOfficeDetails | Observable<IOfficeDetails> | Promise<IOfficeDetails> {
    const id = route.paramMap.get('id');
    return this.officeService.get(id)
      .pipe(map(office => {
        if (office) {
          return office;
        } else {
          this.router.navigate(['/admin/offices']);
          return null;
        }
      }));
  }
}