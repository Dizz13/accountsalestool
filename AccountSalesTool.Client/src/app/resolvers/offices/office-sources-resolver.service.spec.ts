import { TestBed } from '@angular/core/testing';

import { OfficeSourcesResolverService } from './office-sources-resolver.service';

describe('OfficeSourcesResolverService', () => {
  let service: OfficeSourcesResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OfficeSourcesResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
