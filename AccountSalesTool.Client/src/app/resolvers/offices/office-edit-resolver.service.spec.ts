import { TestBed } from '@angular/core/testing';

import { OfficeEditResolverService } from './office-edit-resolver.service';

describe('OfficeEditResolverService', () => {
  let service: OfficeEditResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OfficeEditResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
