import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ISelectResult } from 'src/app/interfaces/select-result';
import { SourceService } from 'src/app/services/source.service';

@Injectable({
  providedIn: 'root'
})
export class SourcesListResolverService implements Resolve<ISelectResult[]> {

  constructor(
    private sourceService: SourceService,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): ISelectResult[] | Observable<ISelectResult[]> | Promise<ISelectResult[]> {
    return this.sourceService.getForSelection()
      .pipe(map(sources => {
        if (sources) {
          return sources;
        } else {
          this.router.navigate(['/admin/sources']);
          return null;
        }
      }));
  }
}
