import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { SourceService } from 'src/app/services/source.service';
import { map } from 'rxjs/operators';
import { ISourceEdit } from 'src/app/interfaces/sources/source-edit';

@Injectable({
  providedIn: 'root'
})
export class EditSourceResolverService implements Resolve<ISourceEdit> {

  constructor(
    private sourceService: SourceService,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): ISourceEdit | Observable<ISourceEdit> | Promise<ISourceEdit> {
    const id = route.paramMap.get('id');
    return this.sourceService.get(id)
      .pipe(map(source => {
        if (source) {
          return source;
        } else {
          this.router.navigate(['/admin/sources']);
          return null;
        }
      }));
  }
}
