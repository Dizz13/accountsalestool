import { TestBed } from '@angular/core/testing';

import { SourcesListResolverService } from './sources-list-resolver.service';

describe('SourcesListResolverService', () => {
  let service: SourcesListResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SourcesListResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
