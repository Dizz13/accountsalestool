import { TestBed } from '@angular/core/testing';

import { EditSourceResolverService } from './edit-source-resolver.service';

describe('EditUserResolverService', () => {
  let service: EditSourceResolverService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EditSourceResolverService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
