import { Injectable, EventEmitter } from '@angular/core';

import { HubConnection, HubConnectionBuilder } from "@aspnet/signalr";

import { environment } from 'src/environments/environment';
import { AuthService } from '../auth/auth.service';

import { IOfficeTicker } from '../interfaces/offices/office-ticker';
import { IIdResult } from '../interfaces/id-result';


@Injectable({
  providedIn: 'root'
})
export class TickerService {

  private hubConnection: HubConnection;

  public data: IOfficeTicker[];
  public dataRecievedEvent: EventEmitter<any> = new EventEmitter();
  public updateRecievedEvent: EventEmitter<string> = new EventEmitter();

  constructor(
    private authService: AuthService
  ) { }

  public startConnection(): void {
    this.hubConnection = new HubConnectionBuilder().withUrl('http://localhost:5000/ticker', { accessTokenFactory: () => this.authService.getToken() }).build();
    this.hubConnection.start().catch(error => alert(error));
  }

  public stopConnection(): void {
    this.hubConnection.stop();
  }

  public addTickerListener(): void {
    this.hubConnection.on('data', (data: IOfficeTicker[]) => {
      this.data = data;
      this.dataRecievedEvent.emit()
    })
    this.hubConnection.on('update', (update: IIdResult) => {
      this.data.find(x => x.id == update.id).sales++;
      this.updateRecievedEvent.emit(update.id);
    })
  }
}
