import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IState } from '../interfaces/states/state';
import { map } from 'rxjs/operators';


@Injectable({
    providedIn: 'root'
})

export class StateService {
    private apiPath = environment.apiUrl + 'states';

    constructor(
        private http: HttpClient
    ) { }

    getAll(): Observable<IState[]> {
        return this.http.get<IState[]>(this.apiPath)
          .pipe(map(result => {
            return result;
          }));
      }
}