import { StateService } from './state.service';
import { TestBed } from '@angular/core/testing';

describe('SalesService', () => {
  let service: StateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});